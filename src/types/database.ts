import { Merge, SetNonNullable, type MergeDeep } from "type-fest";
import { type Database as GeneratedDatabase } from "./database-generated";

export interface User {
	nickname: string;
	hide_info: boolean;
	avatar_url: string;
}

type Post = GeneratedDatabase["public"]["Views"]["posts_with_meta"]["Row"];

export type PostForView = Omit<
	SetNonNullable<Post, keyof Post>,
	"created_at"
> & {
	children: PostForView[];
	created_at: Date;
};

/** Augment and specify more granularly the types generated found in `./database-generated.ts` */
export type Database = GeneratedDatabase;
