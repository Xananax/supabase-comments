export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[];

export interface Database {
  public: {
    Tables: {
      moderated_posts: {
        Row: {
          created_at: string;
          is_hidden: boolean;
          is_pinned: boolean;
          moderator_id: string | null;
          notes: string | null;
          post_id: number;
          updated_at: string;
        };
        Insert: {
          created_at?: string;
          is_hidden?: boolean;
          is_pinned?: boolean;
          moderator_id?: string | null;
          notes?: string | null;
          post_id: number;
          updated_at?: string;
        };
        Update: {
          created_at?: string;
          is_hidden?: boolean;
          is_pinned?: boolean;
          moderator_id?: string | null;
          notes?: string | null;
          post_id?: number;
          updated_at?: string;
        };
        Relationships: [
          {
            foreignKeyName: "moderated_posts_moderator_id_fkey";
            columns: ["moderator_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "moderated_posts_post_id_fkey";
            columns: ["post_id"];
            referencedRelation: "posts";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "moderated_posts_post_id_fkey";
            columns: ["post_id"];
            referencedRelation: "posts_with_meta";
            referencedColumns: ["post_id"];
          }
        ];
      };
      posts: {
        Row: {
          body_raw: string;
          created_at: string;
          deleted_at: string | null;
          is_published: boolean;
          post_id: number;
          post_slug: string;
          post_tags: number[] | null;
          previous_versions: string[];
          reply_to_id: number | null;
          title: string;
          updated_at: string;
          user_id: string | null;
        };
        Insert: {
          body_raw?: string;
          created_at?: string;
          deleted_at?: string | null;
          is_published?: boolean;
          post_id?: never;
          post_slug: string;
          post_tags?: number[] | null;
          previous_versions?: string[];
          reply_to_id?: number | null;
          title: string;
          updated_at?: string;
          user_id?: string | null;
        };
        Update: {
          body_raw?: string;
          created_at?: string;
          deleted_at?: string | null;
          is_published?: boolean;
          post_id?: never;
          post_slug?: string;
          post_tags?: number[] | null;
          previous_versions?: string[];
          reply_to_id?: number | null;
          title?: string;
          updated_at?: string;
          user_id?: string | null;
        };
        Relationships: [
          {
            foreignKeyName: "posts_reply_to_id_fkey";
            columns: ["reply_to_id"];
            referencedRelation: "posts";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "posts_reply_to_id_fkey";
            columns: ["reply_to_id"];
            referencedRelation: "posts_with_meta";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "posts_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
      profiles: {
        Row: {
          avatar_url: string | null;
          created_at: string;
          hide_info: boolean;
          nickname: string | null;
          stripe_customer: string | null;
          updated_at: string;
          user_id: string;
        };
        Insert: {
          avatar_url?: string | null;
          created_at?: string;
          hide_info?: boolean;
          nickname?: string | null;
          stripe_customer?: string | null;
          updated_at?: string;
          user_id: string;
        };
        Update: {
          avatar_url?: string | null;
          created_at?: string;
          hide_info?: boolean;
          nickname?: string | null;
          stripe_customer?: string | null;
          updated_at?: string;
          user_id?: string;
        };
        Relationships: [
          {
            foreignKeyName: "profiles_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
      tags: {
        Row: {
          tag_id: number;
          tag_slug: string;
        };
        Insert: {
          tag_id?: never;
          tag_slug: string;
        };
        Update: {
          tag_id?: never;
          tag_slug?: string;
        };
        Relationships: [];
      };
      tickets: {
        Row: {
          body_raw: string;
          created_at: string;
          flagged_reason: string | null;
          name: string | null;
          notes: string | null;
          reason: string | null;
          sent_at: string;
          status: Database["public"]["Enums"]["ticket_status"];
          ticket_id: number;
          title: string | null;
          user_email: string;
          user_id: string | null;
        };
        Insert: {
          body_raw?: string;
          created_at?: string;
          flagged_reason?: string | null;
          name?: string | null;
          notes?: string | null;
          reason?: string | null;
          sent_at: string;
          status?: Database["public"]["Enums"]["ticket_status"];
          ticket_id?: never;
          title?: string | null;
          user_email: string;
          user_id?: string | null;
        };
        Update: {
          body_raw?: string;
          created_at?: string;
          flagged_reason?: string | null;
          name?: string | null;
          notes?: string | null;
          reason?: string | null;
          sent_at?: string;
          status?: Database["public"]["Enums"]["ticket_status"];
          ticket_id?: never;
          title?: string | null;
          user_email?: string;
          user_id?: string | null;
        };
        Relationships: [
          {
            foreignKeyName: "tickets_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
      tickets_responses: {
        Row: {
          body_raw: string;
          created_at: string;
          moderator_id: string | null;
          notes: string | null;
          response_id: number;
          ticket_id: number;
          user_id: string | null;
        };
        Insert: {
          body_raw?: string;
          created_at?: string;
          moderator_id?: string | null;
          notes?: string | null;
          response_id?: never;
          ticket_id: number;
          user_id?: string | null;
        };
        Update: {
          body_raw?: string;
          created_at?: string;
          moderator_id?: string | null;
          notes?: string | null;
          response_id?: never;
          ticket_id?: number;
          user_id?: string | null;
        };
        Relationships: [
          {
            foreignKeyName: "tickets_responses_moderator_id_fkey";
            columns: ["moderator_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          },
          {
            foreignKeyName: "tickets_responses_ticket_id_fkey";
            columns: ["ticket_id"];
            referencedRelation: "tickets";
            referencedColumns: ["ticket_id"];
          },
          {
            foreignKeyName: "tickets_responses_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
      votes: {
        Row: {
          created_at: string;
          post_id: number;
          updated_at: string;
          user_id: string;
          vote_value: number;
        };
        Insert: {
          created_at?: string;
          post_id: number;
          updated_at?: string;
          user_id: string;
          vote_value?: number;
        };
        Update: {
          created_at?: string;
          post_id?: number;
          updated_at?: string;
          user_id?: string;
          vote_value?: number;
        };
        Relationships: [
          {
            foreignKeyName: "votes_post_id_fkey";
            columns: ["post_id"];
            referencedRelation: "posts";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "votes_post_id_fkey";
            columns: ["post_id"];
            referencedRelation: "posts_with_meta";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "votes_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
    };
    Views: {
      posts_slugs: {
        Row: {
          post_slug: string | null;
        };
        Relationships: [];
      };
      posts_with_meta: {
        Row: {
          avatar_url: string | null;
          body_raw: string | null;
          created_at: string | null;
          deleted_at: string | null;
          hide_info: boolean | null;
          is_author: boolean | null;
          is_deleted: boolean | null;
          is_hidden: boolean | null;
          is_pinned: boolean | null;
          is_published: boolean | null;
          nickname: string | null;
          post_id: number | null;
          post_slug: string | null;
          post_tags: number[] | null;
          previous_versions: string[] | null;
          reply_to_id: number | null;
          self_score: number | null;
          tags_slugs: string[] | null;
          title: string | null;
          updated_at: string | null;
          user_id: string | null;
          vote_score: number | null;
        };
        Relationships: [
          {
            foreignKeyName: "posts_reply_to_id_fkey";
            columns: ["reply_to_id"];
            referencedRelation: "posts";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "posts_reply_to_id_fkey";
            columns: ["reply_to_id"];
            referencedRelation: "posts_with_meta";
            referencedColumns: ["post_id"];
          },
          {
            foreignKeyName: "posts_user_id_fkey";
            columns: ["user_id"];
            referencedRelation: "users";
            referencedColumns: ["id"];
          }
        ];
      };
    };
    Functions: {
      create_post: {
        Args: {
          body_raw: string;
          reply_to_id: number;
          user_id?: string;
        };
        Returns: {
          body_raw: string;
          created_at: string;
          deleted_at: string | null;
          is_published: boolean;
          post_id: number;
          post_slug: string;
          post_tags: number[] | null;
          previous_versions: string[];
          reply_to_id: number | null;
          title: string;
          updated_at: string;
          user_id: string | null;
        };
      };
      create_thread: {
        Args: {
          title: string;
          post_slug: string;
          body_raw: string;
          post_tags?: string[];
          user_id?: string;
        };
        Returns: {
          body_raw: string;
          created_at: string;
          deleted_at: string | null;
          is_published: boolean;
          post_id: number;
          post_slug: string;
          post_tags: number[] | null;
          previous_versions: string[];
          reply_to_id: number | null;
          title: string;
          updated_at: string;
          user_id: string | null;
        };
      };
      create_ticket: {
        Args: {
          user_email: string;
          body_raw: string;
          title?: string;
          sent_at?: string;
          name?: string;
          user_id?: string;
        };
        Returns: undefined;
      };
      delete_claim: {
        Args: {
          uid: string;
          claim: string;
        };
        Returns: string;
      };
      delete_post: {
        Args: {
          post_id: number;
        };
        Returns: undefined;
      };
      elevate_to_claim_admin: {
        Args: {
          uid: string;
        };
        Returns: string;
      };
      get_claim: {
        Args: {
          uid: string;
          claim: string;
        };
        Returns: Json;
      };
      get_claims: {
        Args: {
          uid: string;
        };
        Returns: Json;
      };
      get_my_claim: {
        Args: {
          claim: string;
        };
        Returns: Json;
      };
      get_my_claims: {
        Args: Record<PropertyKey, never>;
        Returns: Json;
      };
      hide_post: {
        Args: {
          post_id: number;
          hide_value: boolean;
          user_id?: string;
        };
        Returns: string;
      };
      is_claims_admin: {
        Args: Record<PropertyKey, never>;
        Returns: boolean;
      };
      is_moderator: {
        Args: Record<PropertyKey, never>;
        Returns: boolean;
      };
      is_post_update_valid: {
        Args: {
          post_id: number;
          reply_to_id: number;
        };
        Returns: boolean;
      };
      pin_post: {
        Args: {
          post_id: number;
          pin_value: boolean;
          user_id?: string;
        };
        Returns: string;
      };
      set_claim: {
        Args: {
          uid: string;
          claim: string;
          value: Json;
        };
        Returns: string;
      };
      set_moderator: {
        Args: {
          uid: string;
        };
        Returns: string;
      };
      set_role: {
        Args: {
          uid: string;
          role: string;
        };
        Returns: string;
      };
      vote: {
        Args: {
          post_id: number;
          vote_value?: number;
          user_id?: string;
        };
        Returns: undefined;
      };
    };
    Enums: {
      ticket_status: "WAITING" | "PROCESSING" | "HALTED" | "ARCHIVED";
    };
    CompositeTypes: {
      [_ in never]: never;
    };
  };
}
