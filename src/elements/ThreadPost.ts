import { $convertFromMarkdownString, TRANSFORMERS } from "@lexical/markdown";
import {
	tmpl as html,
	WebComponent,
	ul,
	li,
	div,
	h2,
	h,
	bindLexical,
	button,
	span,
} from "../framework";
import { PostForView } from "../types/database";
import { LexicalEditor } from "lexical";

export type ThreadPostEvents = {
	"reply-request": { post_id: number; title: string };
	"delete-request": { post_id: number };
	"vote-request": { post_id: number; vote_value: 1 | 0 | -1 };
	"pin-request": { post_id: number; pin_value: boolean };
	"hide-request": { post_id: number; hide_value: boolean };
	"edit-request": {
		post_id: number;
		reply_to_id: number;
		title: string;
		body_raw: string;
	};
};

declare global {
	interface HTMLElementTagNameMap {
		"thread-post": ThreadPost;
	}
}

export class ThreadPost extends WebComponent("thread-post")
	.observedAttributes("hide-title")
	.events<ThreadPostEvents>()
	.template(
		html`
			<style>
				ul,
				li {
					list-style: none;
					padding: 0;
				}
				:host {
					position: relative;
				}
				:host::part(author-buttons-list),
				:host::part(user-buttons-list),
				:host::part(moderator-buttons-list),
				:host::part(info) {
					display: flex;
					flex-wrap: wrap;
					gap: 0.1em;
				}
				:host::part(info) {
					font-size: 0.8em;
					gap: 1em;
				}
				::slotted([slot="pinned"]) {
					position: absolute;
					top: 0;
					left: 0;
				}
			</style>
			<slot name="pinned"></slot>
			<slot name="title"></slot>
			<div part="body"><slot name="body"></slot></div>
			<div part="info"><slot name="info"></slot></div>
			<nav part="author-buttons-list">
				<slot name="author-buttons-list"></slot>
			</nav>
			<nav part="user-buttons-list">
				<slot name="user-buttons-list"></slot>
			</nav>
			<nav part="moderator-buttons-list">
				<slot name="moderator-buttons-list"></slot>
			</nav>
			<div part="children"><slot name="children"></slot></div>
		`
	).Class {
	_data: PostForView | null = null;
	_userId?: string;
	_moderationMode = false;
	editor: LexicalEditor | null = null;

	constructor() {
		super();
		this._instantiateTemplate();
	}

	set data(newData: PostForView | null) {
		this._data = newData;
		this._requestUpdate();
	}

	get data(): PostForView | null {
		return this._data;
	}

	set userId(value: string | undefined) {
		this._userId = value;
		this._requestUpdate();
	}

	get userId(): string | undefined {
		return this._userId;
	}

	set moderationMode(value: boolean) {
		this._moderationMode = value;
		this._requestUpdate();
	}

	get moderationMode() {
		return this._moderationMode;
	}

	get hideTitle() {
		return this.hasAttribute("hide-title");
	}

	set hideTitle(val: boolean | string | null) {
		this.setBooleanAttribute("hide-title", val);
	}

	get pinned() {
		return this.hasAttribute("pinned");
	}

	set pinned(val: boolean | string | null) {
		this.setBooleanAttribute("pinned", val);
	}

	attributeChangedCallback(
		name: "hide-title",
		_oldValue: string,
		_newValue: string
	): void {
		switch (name) {
			case "hide-title":
				this._requestUpdate();
				break;
		}
	}

	update() {
		this.clearLightDom();
		if (this._data == null) {
			return;
		}
		const {
			body_raw,
			title,
			children,
			is_author,
			post_id,
			reply_to_id,
			created_at,
			vote_score,
			self_score,
			nickname,
			is_pinned,
			is_hidden,
		} = this._data;

		is_pinned && span({ slot: "pinned" }, "📌").appendTo(this);

		const textElement = div({ slot: "body" }, body_raw).appendTo(this);
		this.editor = bindLexical(textElement);
		this.editor.update(() =>
			$convertFromMarkdownString(body_raw, TRANSFORMERS)
		);

		span({ slot: "info" }, new Date(created_at).toLocaleString()).appendTo(
			this
		);
		span({ slot: "info" }, nickname).appendTo(this);

		vote_score && span({ slot: "info" }, `♥${vote_score}`).appendTo(this);
		self_score && span({ slot: "info" }, `me: ♥${self_score}`).appendTo(this);

		this._userId &&
			button(
				{
					slot: "user-buttons-list",
					className: ["thread-post__user-button"],
					onClick: () =>
						this._data &&
						this.dispatchCustomEvent(`reply-request`, {
							post_id,
							title,
						}),
				},
				"Reply"
			).appendTo(this);

		this._userId &&
			button(
				{
					slot: "user-buttons-list",
					className: "thread-post__user-button",
					title: self_score ? "downvote" : "upvote",
					onClick: () =>
						this._data &&
						this.dispatchCustomEvent(`vote-request`, {
							post_id,
							vote_value: self_score ? 0 : 1,
						}),
				},
				self_score ? "🡇" : "🡅"
			).appendTo(this);

		is_author &&
			(["edit", "delete"] as const).forEach((action) =>
				button(
					{
						slot: "author-buttons-list",
						className: "thread-post__author-button",
						onClick: () =>
							this.dispatchCustomEvent(`${action}-request`, {
								post_id,
								reply_to_id,
								title,
								body_raw,
							}),
					},
					action
				).appendTo(this)
			);

		this._moderationMode &&
			(["edit", "delete"] as const).forEach((action) =>
				button(
					{
						slot: "moderator-buttons-list",
						className: "thread-post__moderator-button",
						onClick: () =>
							this.dispatchCustomEvent(`${action}-request`, {
								post_id,
								reply_to_id,
								title,
								body_raw,
							}),
					},
					action
				).appendTo(this)
			);
		this._moderationMode &&
			button(
				{
					slot: "moderator-buttons-list",
					className: "thread-post__moderator-button",
					onClick: () =>
						this.dispatchCustomEvent(`pin-request`, {
							post_id,
							pin_value: !is_pinned,
						}),
				},
				is_pinned ? "unpin" : "pin"
			).appendTo(this);

		this._moderationMode &&
			button(
				{
					slot: "moderator-buttons-list",
					className: "thread-post__moderator-button",
					onClick: () =>
						this.dispatchCustomEvent(`hide-request`, {
							post_id,
							hide_value: !is_hidden,
						}),
				},
				is_hidden ? "unhide" : "hide"
			).appendTo(this);

		!this.hideTitle && title && h2({ slot: "title" }, title).appendTo(this);

		children &&
			ul(
				{ slot: "children" },
				...children.map((data) =>
					li(
						{},
						h("thread-post", {
							props: {
								moderationMode: this.moderationMode,
								userId: this.userId,
								data,
							},
							// @ts-ignore TODO: type props and attributes
							"hide-title": true,
							exportparts:
								"body,children,reply-to-post,author-buttons-list,user-buttons-list,admin-buttons-list,info",
						})
					)
				)
			).appendTo(this);
	}
}
