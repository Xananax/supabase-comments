import { ThreadPost } from "./ThreadPost";
import {
	tmpl as html,
	WebComponent,
	ul,
	li,
	h,
	createElementTemplate,
} from "../framework";
import { ThreadPostEvents } from "./ThreadPost";
import { PostForView } from "../types/database";

interface ThreadPostListEvents extends ThreadPostEvents {}

declare global {
	interface HTMLElementTagNameMap {
		"thread-post-list": ThreadPostList;
	}
}

export class ThreadPostList extends WebComponent("thread-post-list")
	.events<ThreadPostListEvents>()
	.observedAttributes("empty").template(html`
	<style>
		:host {
			display: block;
		}
		:host([hidden]) {
			display: none;
		}
		. :host::part(empty) {
			display: none;
		}
		:host([empty])::part(empty) {
			display: block;
		}
	</style>
	<div part="list">
		<slot name="list"></slot>
	</div>
	<div part="empty"><slot name="empty"></slot></div>
`).Class {
	_userId?: string;
	_moderationMode = false;
	_data: PostForView[] | null = null;
	$list: HTMLUListElement | null = null;
	_dispatchEvent = this.dispatchEvent.bind(this);

	constructor() {
		super();
		this._instantiateTemplate();
	}

	set data(newData: PostForView[] | null) {
		this._data = newData;
		this._requestUpdate();
	}

	get data(): PostForView[] | null {
		return this._data;
	}

	set userId(value: string | undefined) {
		this._userId = value;
		this._requestUpdate();
	}

	get userId(): string | undefined {
		return this._userId;
	}

	set moderationMode(value: boolean) {
		this._moderationMode = value;
		this._requestUpdate();
	}

	get moderationMode() {
		return this._moderationMode;
	}

	get empty() {
		return this.hasAttribute("empty");
	}

	set empty(val: boolean | string | null) {
		this.setBooleanAttribute("empty", val);
	}

	update() {
		this.$list && this.removeChild(this.$list);
		this.$list = null;
		if (this._data == null) {
			return;
		}

		this.$list = ul(
			{ slot: "list" },
			...this._data.map((data) =>
				li(
					{},
					h("thread-post", {
						props: {
							moderationMode: this.moderationMode,
							userId: this.userId,
							data,
						},
						exportparts:
							"body,children,reply-to-post,author-buttons-list,user-buttons-list,admin-buttons-list,info",
					})
				)
			)
		).appendTo(this);
	}
}
