import {
	tmpl as html,
	WebComponent,
	bindLexical,
	toggleRenderedMarkdown,
} from "../framework";
import { type LexicalEditor } from "lexical";
import {
	$convertFromMarkdownString,
	$convertToMarkdownString,
} from "@lexical/markdown";
import { TRANSFORMERS } from "../framework/markdownTransformers";

declare global {
	interface HTMLElementTagNameMap {
		"rich-text-editor": RichTextEditor;
	}
}

const isContentEditable = (el: any): el is HTMLElement =>
	el != null && el instanceof HTMLElement && el.isContentEditable === true;

export class RichTextEditor extends WebComponent(
	"rich-text-editor"
).observedAttributes("value", "name", "plain", "required").template(html`
	<style>
		:host::part(editor-pane-container) {
			background-color: red;
			width: 100%;
			min-height: 8em;
			position: relative;
			display: flex;
			align-items: stretch;
			align-content: stretch;
		}
		::slotted(*) {
			width: 100%;
			height: 100%;
			min-height: 8em;
			background: orange;
		}
	</style>
	<div part="editor-container">
		<div part="editor-buttons">
			<button part="button-source">source</button>
		</div>
		<div part="editor-pane-container">
			<slot></slot>
		</div>
	</div>
`).Class {
	static formAssociated = true;

	internals = this.attachInternals();
	_editor: LexicalEditor | null = null;
	_removeTextContentListener: (() => void) | null = null;
	markdown = "";
	$editorContainer = this.shadow.querySelector('div[part="editor-container"]');

	constructor() {
		super();
		this.internals.ariaRoleDescription = "a text editor";
		this.setAttribute("role", "textbox");
		// we need this until we can use web components with lexical, see: https://github.com/facebook/lexical/issues/2119
		this.shadow.addEventListener("slotchange", this._onSlotChange.bind(this));
		this.shadow.querySelectorAll("button").forEach((button) => {
			switch (button.textContent) {
				case "source":
					button.addEventListener("click", this.togglePlain.bind(this));
					break;
			}
		});
	}

	_onSlotChange(evt: Event) {
		if (evt.target instanceof HTMLSlotElement) {
			this._removeTextContentListener && this._removeTextContentListener();
			const slot = evt.target;
			const contentEditableElement = slot
				.assignedElements()
				.find(isContentEditable);
			if (contentEditableElement) {
				this._editor = bindLexical(contentEditableElement);
				this._removeTextContentListener = this._editor.registerUpdateListener(
					({ editorState }) => {
						editorState.read(() => {
							this.markdown = $convertToMarkdownString(TRANSFORMERS);
							this._setValidity();
						});
					}
				);
				this._setValidity();
			}
		}
	}

	disconnectedCallback() {
		this._removeTextContentListener && this._removeTextContentListener();
	}

	get value() {
		return this.markdown;
	}

	set value(value: string) {
		this.markdown = value;
		this._editor?.update(() => {
			$convertFromMarkdownString(value, TRANSFORMERS);
		});
		this.internals.setFormValue(value);
	}

	togglePlain() {
		this.plain = !this.plain;
	}

	_setValidity() {
		this.internals.setValidity(
			{
				valueMissing: this.required && this.markdown === "",
			},
			"Please fill this field"
		);
	}

	attributeChangedCallback(name: string, oldValue: any, newValue: any) {
		if (oldValue === newValue) {
			return;
		}
		switch (name) {
			case "value":
				this.value = newValue;
				break;
			case "plain":
				this._editor && toggleRenderedMarkdown(this._editor);
				break;
		}
		this._setValidity();
	}

	/////////// Reflected attributes

	set name(val: string) {
		this.setAttribute("name", val);
	}

	get name() {
		return this.getAttribute("name") ?? "";
	}

	set required(val: boolean) {
		this.setBooleanAttribute("required", val);
	}

	get required() {
		return this.hasAttribute("required");
	}

	set plain(val: boolean) {
		this.setBooleanAttribute("plain", val);
	}

	get plain() {
		return this.hasAttribute("plain");
	}
}
