import { tmpl as html, WebComponent } from "../framework";

let ids = 0;

declare global {
	interface HTMLElementTagNameMap {
		"form-field": FormField;
	}
}

export class FormField extends WebComponent("form-field").observedAttributes(
	"name",
	"id",
	"type",
	"disabled",
	"label",
	"placeholder",
	"value",
	"required"
).template(html` <style>
		:host {
			display: flex;
		}
		:host([hidden]) {
			display: none;
		}
		. :host::part(label) {
			display: flex;
			align-items: center;
			gap: var(--gap);
		}
	</style>
	<label part="label">
		<span part="label-name"><slot name="label"></slot></span>
		<span part="input-container">
			<slot name="input">
				<input part="input" type="text" />
			</slot>
		</span>
	</label>`).Class {
	static formAssociated = true;

	_internalId = `id_${++ids}`;

	internals: ElementInternals = this.attachInternals();
	$label: HTMLLabelElement | null = this.shadow.querySelector("label");
	$span: HTMLSpanElement | null =
		this.shadow.querySelector("span:first-of-type");
	$input: HTMLInputElement | null | HTMLTextAreaElement =
		this.shadow.querySelector("input, textarea");

	connectedCallback() {
		this._setValidity();
		this.$input?.addEventListener("input", this._setValidity.bind(this));
	}

	_setValidity() {
		if (!this.$input) {
			throw new Error("no input child found");
		}
		this.internals.setValidity(
			this.$input.validity,
			this.$input.validationMessage,
			this.$input
		);
	}

	get name() {
		return this.$input?.getAttribute("name") ?? "";
	}

	set name(val: string) {
		this.$input?.setAttribute("name", val);
	}

	get label() {
		return this.getAttribute("label") ?? "";
	}

	set label(val: string) {
		this.$span && (this.$span.textContent = val);
	}

	get id() {
		return this.$input?.getAttribute("id") ?? "";
	}

	set id(val: string) {
		this.$input?.setAttribute("id", `${val}__input__${this._internalId}`);
		this.$label?.setAttribute("for", `${val}__input__${this._internalId}`);
	}

	get type() {
		return this.$input?.getAttribute("type") ?? "";
	}

	set type(val: string) {
		this.setAttribute("type", val);
		this.$input?.setAttribute("type", val);
	}

	get placeholder() {
		return this.getAttribute("placeholder") ?? "";
	}

	set placeholder(val: string) {
		this.setAttribute("placeholder", val);
		this.$input?.setAttribute("placeholder", val);
	}

	get value() {
		return this.$input?.value ?? "";
	}

	set value(val: string) {
		this.internals.setFormValue(val);
		this.$input && (this.$input.value = val);
	}

	get required() {
		return this.hasAttribute("required");
	}

	set required(val: boolean | null | string) {
		this.setBooleanAttribute("required", val);
	}

	get disabled() {
		return this.hasAttribute("disabled");
	}

	set disabled(val: boolean | string | null) {
		this.setBooleanAttribute("disabled", val);
	}

	attributeChangedCallback(
		name:
			| "name"
			| "id"
			| "type"
			| "disabled"
			| "label"
			| "placeholder"
			| "value"
			| "required",
		oldValue: string,
		newValue: string
	): void {
		if (oldValue === newValue) {
			return;
		}
		switch (name) {
			// proxied values
			case "name":
			case "id":
			case "label":
			case "placeholder":
			case "value":
				this[name] = newValue;
		}
		this._setValidity();
	}
}
