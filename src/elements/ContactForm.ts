import { WebComponent, tmpl as html } from "../framework";
import { FormField } from "./FormField";
import { RichTextEditor } from "./RichTextEditor";

interface ContactFormEvents {
	submit: ContactFormData;
}

interface ContactFormElements extends HTMLCollection {
	title: HTMLInputElement;
	email: HTMLInputElement;
	body: HTMLInputElement;
	name: HTMLInputElement;
}

declare global {
	interface HTMLElementTagNameMap {
		"contact-form": ContactForm;
	}
}

interface ContactFormData {
	user_id?: string;
	user_email: string;
	body_raw: string;
	/** This should be set to the time the form was loaded */
	sent_at: string;
	/** this should NOT be filled. It HAS to be null or empty string */
	name?: string;
	title?: string;
}

export class ContactForm extends WebComponent("contact-form")
	.events<ContactFormEvents>()
	.observedAttributes("replyto").template(html`
	<style>
		form {
			display: flex;
			flex-direction: column;
			align-items: stretch;
			align-content: space-between;
			justify-content: space-between;
			gap: 0.3em;
		}
		:host([hide-title])::part(title-field) {
			display: none;
		}
	</style>
	<form>
		<form-field
			name="title"
			type="text"
			placeholder="Title"
			part="title-field"
		></form-field>
		<form-field
			name="email"
			type="email"
			required
			placeholder="mail@provider.tld"
			part="email-field"
		></form-field>
		<div
			aria-hidden
			style="opacity: 0; position: absolute; top: 0; left: 0; height: 0; width: 0; z-index: -1;"
			placeholder="your synth name, as a replicant"
		>
			<input type="text" autocomplete="off" name="name" />
		</div>
		<rich-text-editor
			name="body"
			required
			exportparts="editor-container,editor-buttons,button-source,editor-pane-container"
		>
			<div contenteditable="true"></div>
		</rich-text-editor>
		<slot></slot>
		<input type="submit" value="post" part="button" />
	</form>
`).Class {
	sent_at: Date = new Date();
	user_id?: string;
	$form = this.shadow.querySelector<HTMLFormElement>("form")!;
	$title = this.shadow.querySelector<FormField>("form-field")!;
	$richTextEditor =
		this.shadow.querySelector<RichTextEditor>("rich-text-editor")!;

	constructor() {
		super();
		this.$form.addEventListener("submit", (evt) => {
			evt.preventDefault();
			const elements = (evt.target as HTMLFormElement)
				.elements as ContactFormElements;
			const data = this.getFormData();
			this.dispatchCustomEvent("submit", data);
		});
	}

	getFormData(): ContactFormData {
		const elements = this.$form.elements as ContactFormElements;

		return {
			user_id: this.user_id,
			user_email: elements.email.value,
			body_raw: elements.body.value,
			title: elements.title.value,
			sent_at: this.sent_at.toISOString(),
			name: elements.name.value,
		};
	}

	setTimeToNow() {
		this.sent_at = new Date();
	}
}
