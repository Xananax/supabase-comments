import { tmpl as html, WebComponent } from "../framework";

declare global {
	interface HTMLElementTagNameMap {
		"user-form": UserForm;
	}
}

interface UserFormEvents {
	"login-attempt": { email: string };
	"logout-attempt": null;
}

declare global {
	interface HTMLElementTagNameMap {
		"user-form": UserForm;
	}
}

export class UserForm extends WebComponent("user-form")
	.events<UserFormEvents>()
	.observedAttributes("loggedin", "disabled").template(html`
	<style>
		:host {
			display: flex;
			align-items: center;
			padding: var(--gap);
		}
		:host([hidden]) {
			display: none;
		}
		. :host::part(loggedin) {
			display: flex;
		}
		:host::part(loggedout) {
			display: none;
		}
		:host([disabled]) {
			opacity: 0.5;
			form,
			button,
			input,
			a {
				cursor: not-allowed;
			}
		}
		:host([loggedin])::part(loggedin) {
			display: none;
		}
		:host([loggedin])::part(loggedout) {
			display: flex;
		}
	</style>
	<form part="loggedin">
		<fieldset part="fieldset">
			<form-field
				name="email"
				type="email"
				placeholder="email@provider.com"
				exportparts="label,input"
			></form-field>
			<input part="button" type="submit" value="ok" />
		</fieldset>
	</form>
	<div part="loggedout">
		<span part="user-name"></span>
		<a part="link" href="#logout">log out</a>
	</div>
`).Class {
	$form = this.shadow.querySelector<HTMLFormElement>("form");
	$emailField = this.shadow.querySelector<HTMLInputElement>(
		'form-field[type="email"]'
	);
	$logoutLink = this.shadow.querySelector<HTMLAnchorElement>("a");
	$fieldset = this.shadow.querySelector<HTMLFieldSetElement>("fieldset");

	constructor() {
		super();

		this.$form?.addEventListener("submit", (evt) => {
			evt.preventDefault();
			if (this.loggedin || this.disabled) {
				return;
			}
			this.$emailField?.value &&
				this.dispatchCustomEvent("login-attempt", {
					email: this.$emailField?.value,
				});
		});
		this.$logoutLink?.addEventListener("click", (evt) => {
			evt.preventDefault();
			if (!this.loggedin || this.disabled) {
				return;
			}
			this.dispatchCustomEvent("logout-attempt", null);
		});
	}

	get loggedin() {
		return this.hasAttribute("loggedin");
	}

	set loggedin(val) {
		this.setBooleanAttribute("loggedin", val);
	}

	get disabled() {
		return this.hasAttribute("disabled");
	}

	set disabled(val) {
		this.setBooleanAttribute("disabled", val);
	}

	setFormDisabled(isDisabled: boolean) {
		this.$fieldset && (this.$fieldset.disabled = isDisabled);
		this.$form && (this.$form.ariaDisabled = isDisabled ? "true" : "");
	}

	setLinkDisabled(isDisabled: boolean) {
		this.$logoutLink &&
			(this.$logoutLink.ariaDisabled = isDisabled ? "true" : "");
	}

	attributeChangedCallback(
		name: "loggedin" | "disabled",
		oldValue: string,
		newValue: string
	): void {
		switch (name) {
			case "loggedin":
				const isLoggedIn = newValue !== null;
				if (isLoggedIn) {
					this.setFormDisabled(true);
					this.setLinkDisabled(false);
				} else {
					this.setFormDisabled(false);
					this.setLinkDisabled(true);
				}
				break;
			case "disabled":
				const isEnabled = newValue === null;
				if (isEnabled) {
					if (this.loggedin) {
						this.setFormDisabled(true);
						this.setLinkDisabled(false);
					} else {
						this.setFormDisabled(true);
						this.setLinkDisabled(false);
					}
				} else {
					this.setFormDisabled(true);
					this.setLinkDisabled(true);
				}
				break;
		}
	}
}
