import { WebComponent, tmpl as html } from "../framework";
import { assertUnreachable } from "../framework/assertUnreachable";
import { FormField } from "./FormField";
import { RichTextEditor } from "./RichTextEditor";

interface ThreadFormEvents {
	submit: ThreadFormData;
}

interface ThreadFormElements extends HTMLCollection {
	title: HTMLInputElement;
	body: HTMLInputElement;
}

type ThreadPostModes = "create" | "reply" | "update" | "delete";

type ThreadFormData =
	| {
			title: string;
			post_slug: string;
			body_raw: string;
			mode: "create";
	  }
	| {
			reply_to_id: number;
			body_raw: string;
			mode: "reply";
	  }
	| {
			post_id: number;
			reply_to_id: number;
			body_raw: string;
			title: string;
			mode: "update";
	  }
	| {
			post_id: number;
			mode: "delete";
	  };

const isValidInt = (value: any): value is number =>
	typeof value === "number" && !isNaN(value) && value >= 0;

declare global {
	interface HTMLElementTagNameMap {
		"form-field": FormField;
		"rich-text-editor": RichTextEditor;
		"thread-form": ThreadForm;
	}
}

export class ThreadForm extends WebComponent("thread-form")
	.events<ThreadFormEvents>()
	.observedAttributes("replyto").template(html`
	<style>
		form {
			display: flex;
			flex-direction: column;
			align-items: stretch;
			align-content: space-between;
			justify-content: space-between;
			gap: 0.3em;
		}
		:host([hide-title])::part(title-field) {
			display: none;
		}
	</style>
	<form>
		<form-field
			name="title"
			type="text"
			placeholder="Title"
			part="title-field"
		></form-field>
		<rich-text-editor
			name="body"
			exportparts="editor-container,editor-buttons,button-source,editor-pane-container"
		>
			<div contenteditable="true"></div>
		</rich-text-editor>
		<slot></slot>
		<input type="submit" value="post" part="button" />
	</form>
`).Class {
	$form: HTMLFormElement = this.shadow.querySelector("form")!;
	$title: FormField = this.shadow.querySelector("form-field")!;
	$richTextEditor: RichTextEditor =
		this.shadow.querySelector("rich-text-editor")!;
	mode: ThreadPostModes = "create";

	constructor() {
		super();
		this.$form.addEventListener("submit", (evt) => {
			evt.preventDefault();
			const elements = (evt.target as HTMLFormElement)
				.elements as ThreadFormElements;
			const data = this.getFormData();
			this.dispatchCustomEvent("submit", data);
		});
	}

	getFormData(): ThreadFormData {
		const elements = this.$form.elements as ThreadFormElements;
		const reply_to_id = this.replyTo;
		const post_id = this.postId;
		switch (this.mode) {
			case "create":
				return {
					title: elements.title.value,
					body_raw: elements.body.value,
					post_slug: this.url,
					mode: "create",
				};
			case "reply":
				if (!reply_to_id) {
					throw new Error(`reply without reply_to_id`);
				}
				return {
					body_raw: elements.body.value,
					reply_to_id,
					mode: "reply",
				};
			case "update":
				if (!post_id) {
					throw new Error(`update without post_id`);
				}
				if (!reply_to_id) {
					throw new Error(`reply without reply_to_id`);
				}
				return {
					title: elements.title.value,
					body_raw: elements.body.value,
					post_id,
					reply_to_id,
					mode: "update",
				};
			case "delete":
				if (!post_id) {
					throw new Error(`delete without post_id`);
				}
				return {
					post_id,
					mode: "delete",
				};
			default:
				assertUnreachable(this.mode);
				break;
		}
	}

	set url(value: string) {
		this.setAttribute("url", value);
	}

	get url() {
		return this.getAttribute("url") ?? "";
	}

	set title(value: string) {
		this.$title.value = value;
	}

	get title() {
		return this.$title.value;
	}

	set replyTo(value: number | undefined) {
		isValidInt(value)
			? this.setAttribute("replyto", value + "")
			: this.removeAttribute("replyto");
	}

	get replyTo() {
		const value = parseInt(this.getAttribute("replyto") ?? "");
		return isValidInt(value) ? value : undefined;
	}

	set postId(value: number | undefined) {
		isValidInt(value)
			? this.setAttribute("post_id", value + "")
			: this.removeAttribute("post_id");
	}

	get postId() {
		const value = parseInt(this.getAttribute("post_id") ?? "");
		return isValidInt(value) ? value : undefined;
	}

	get hideTitle() {
		return this.hasAttribute("hide-title");
	}

	set hideTitle(val) {
		this.setBooleanAttribute("hide-title", val);
	}
}
