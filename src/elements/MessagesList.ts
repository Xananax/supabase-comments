import { tmpl as html, WebComponent, ul, li, span, button } from "../framework";
import { PostgrestError } from "@supabase/supabase-js";

interface MessagesListEvent {
	"close-message": { index: number };
}

type Message = Error | PostgrestError | string;

declare global {
	interface HTMLElementTagNameMap {
		"messages-list": MessagesList;
	}
}

export class MessagesList extends WebComponent(
	"messages-list"
).events<MessagesListEvent>().template(html`
	<div part="messages-list-container">
		<slot></slot>
	</div>
`).Class {
	_data: Message[] | null = null;
	$list: HTMLUListElement | null = null;

	constructor() {
		super();
		this._instantiateTemplate();
	}

	set data(newData: Message[] | null) {
		this._data = newData;
		this._requestUpdate();
	}

	get data(): Message[] | null {
		return this._data;
	}

	update() {
		this.$list && this.removeChild(this.$list);
		this.$list = null;
		if (this._data == null) {
			return;
		}
		this.$list = ul(
			{},
			...this._data.map((data, index) => {
				const message = typeof data === "string" ? data : data.message;
				const className =
					typeof data === "string" ? "message" : ["message", "message__error"];
				return li(
					{ className },
					span(null, message + ""),
					button(
						{
							class: "close",
							onClick: (_evt) => {
								this.dispatchCustomEvent("close-message", {
									index,
								});
							},
						},
						"×"
					)
				);
			})
			// @ts-ignore
		).appendTo(this);
	}
}
