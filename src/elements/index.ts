export * from "./ContactForm";
export * from "./FormField";
export * from "./MessagesList";
export * from "./RichTextEditor";
export * from "./ThreadForm";
export * from "./ThreadPost";
export * from "./ThreadPostList";
export * from "./UserForm";

import { ContactForm } from "./ContactForm";
import { FormField } from "./FormField";
import { MessagesList } from "./MessagesList";
import { RichTextEditor } from "./RichTextEditor";
import { ThreadForm } from "./ThreadForm";
import { ThreadPost } from "./ThreadPost";
import { ThreadPostList } from "./ThreadPostList";
import { UserForm } from "./UserForm";

export const elementsAreReady = Promise.all(
	[
		FormField,
		ContactForm,
		MessagesList,
		RichTextEditor,
		ThreadForm,
		ThreadPost,
		ThreadPostList,
		UserForm,
	].map((el) => el.define())
);
