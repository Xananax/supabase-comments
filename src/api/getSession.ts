import { type Session } from "@supabase/supabase-js";
import { supabase } from "./supabase";

export const getSession = ({
	onSessionChanged,
	onError,
}: {
	onSessionChanged: (session: Session | null) => void;
	onError: (error: Error) => void;
}) => {
	async function loginOTP(email: string) {
		const { data, error } = await supabase.auth.signInWithOtp({
			email,
		});
		if (error != null) {
			onError(error);
		} else {
			onSessionChanged(data.session);
		}
		return { data, error };
	}

	async function logout() {
		const { error } = await supabase.auth.signOut();
		if (error) {
			onError(error);
		}
	}

	supabase.auth.getSession().then(({ data: { session: s }, error }) => {
		if (error != null) {
			onError(error);
		} else {
			onSessionChanged(s);
		}
	});

	supabase.auth.onAuthStateChange((_event, s) => {
		onSessionChanged(s);
	});

	return { loginOTP, logout };
};
