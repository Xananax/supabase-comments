import { Database, PostForView } from "../types/database";
import { supabase } from "./supabase";
import { wrapper, handleSupabaseResponse } from "./wrapper";

const sortPosts = (a: PostForView, b: PostForView) =>
	(b.is_pinned ? 1 : 0) - (a.is_pinned ? 1 : 0) ||
	b.vote_score - a.vote_score ||
	b.created_at.getTime() - a.created_at.getTime();

export const getPostsListBySlug = (post_slug: string) =>
	wrapper(
		supabase
			.from("posts_with_meta")
			.select("*")
			.eq("post_slug", post_slug)
			//.eq("is_hidden", false)
			.or("is_hidden.eq.false,is_hidden.is.null") // <-- only useful when not "coalesce"
			.order("is_pinned", { ascending: false })
			.order("vote_score", { ascending: false })
			.order("created_at", { ascending: false })
	).then(({ error, data: rows }) => {
		if (error) {
			throw error;
		}
		// no matter what I tried, recursive queries weren't fast enough in Postgres, so we stitch them here
		const temporaryRowsMapCache = new Map(
			rows.map(
				({ created_at, ...post }) =>
					[
						post.post_id,
						{
							...post,
							created_at: new Date(created_at!),
							children: [] as PostForView[],
						},
					] as const
			)
		);
		const threads: PostForView[] = [];
		temporaryRowsMapCache.forEach((row) => {
			if (!row.reply_to_id) {
				threads.push(row as PostForView);
				return;
			}
			const parent = temporaryRowsMapCache.get(row.reply_to_id);
			if (parent) {
				parent.children.push(row as PostForView);
			}
		});
		/**/
		temporaryRowsMapCache.forEach((row) => {
			row.children.sort(sortPosts);
		});
		threads.sort(sortPosts);
		/**/
		return threads;
	});

export const createThread = (
	args: Database["public"]["Functions"]["create_thread"]["Args"]
) =>
	wrapper(supabase.rpc("create_thread", { ...args })).then(
		handleSupabaseResponse
	);

export const votePost = (post_id: number, vote_value: 1 | 0 | -1) =>
	wrapper(supabase.rpc("vote", { post_id, vote_value })).then(
		handleSupabaseResponse
	);

export const deletePost = (post_id: number) =>
	wrapper(supabase.rpc("delete_post", { post_id })).then(
		handleSupabaseResponse
	);

export const createPost = (
	args: Database["public"]["Functions"]["create_post"]["Args"]
) =>
	wrapper(supabase.rpc("create_post", { ...args })).then(
		handleSupabaseResponse
	);

export const updatePost = ({
	body_raw,
	title,
	post_id,
}: {
	body_raw: string;
	title: string;
	post_id: number;
}) => {
	return wrapper(
		supabase
			.from("posts")
			.update({ body_raw, title })
			.eq("post_id", post_id)
			.select()
	).then(handleSupabaseResponse);
};

export const getAllPostsPages = () =>
	wrapper(supabase.from("posts_slugs").select("post_slug")).then(
		handleSupabaseResponse
	);

export const pinPost = (post_id: number, pin_value = true) =>
	wrapper(supabase.rpc("pin_post", { post_id, pin_value })).then(
		handleSupabaseResponse
	);

export const hidePost = (post_id: number, hide_value = true) =>
	wrapper(supabase.rpc("hide_post", { post_id, hide_value })).then(
		handleSupabaseResponse
	);
