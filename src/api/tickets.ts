import { type Database } from "../types/database";
import { supabase } from "./supabase";
import { wrapper, handleSupabaseResponse } from "./wrapper";

export const createTicket = (
	args: Database["public"]["Functions"]["create_ticket"]["Args"]
) => wrapper(supabase.rpc("create_ticket", args)).then(handleSupabaseResponse);

export const getAllTickets = () =>
	wrapper(supabase.from("tickets").select("*")).then(handleSupabaseResponse);
