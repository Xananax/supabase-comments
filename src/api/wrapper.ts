import { PostgrestSingleResponse } from "@supabase/supabase-js";

export const handleSupabaseResponse = <T>({
	error,
	data,
}: PostgrestSingleResponse<T>) => {
	if (error) {
		console.error(error);
		throw Error;
	}
	console.log(`supabase`, data);
	return data;
};

export const wrapper = <T, P extends PostgrestSingleResponse<T>>(
	s: PromiseLike<P>
) => Promise.resolve(s);
