import { createClient } from "@supabase/supabase-js";
import { Database } from "../types/database";

const { SUPABASE_URL, SUPABASE_ANON_KEY } = process.env;
export const supabase = createClient<Database>(
	SUPABASE_URL!,
	SUPABASE_ANON_KEY!
);
