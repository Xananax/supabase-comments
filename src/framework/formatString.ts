/**
 *
 * @param str A string with `{key}` replacement slots
 * @param replacements An object with replacements, or an array.
 * @returns
 */
export const formatString = (
	str: string,
	replacements?: Array<any> | Record<string, any> | null
) =>
	replacements != null
		? // @ts-expect-error numeric strings can index arrays, so the below works thanks to JS auto-casting magic
		  str.replace(/\{(.*?)\}/g, (_all, key: string) => replacements[key] ?? "")
		: str;
