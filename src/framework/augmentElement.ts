import { appendTo } from "./appendTo";
import { queryElement } from "./queryElement";

/**
 * Augments an element with a few convenience methods.
 * @returns
 */
export const augmentElement = <E extends Element>(element: E) =>
	Object.assign(element, { appendTo, queryElement });
