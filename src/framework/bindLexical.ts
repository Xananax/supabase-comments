import {
	type CreateEditorArgs,
	createEditor,
	LexicalEditor,
	$getRoot,
	$createTextNode,
} from "lexical";
import {
	$createCodeNode,
	$isCodeNode,
	CodeHighlightNode,
	CodeNode,
} from "@lexical/code";
import { LinkNode } from "@lexical/link";
import { registerCodeHighlighting } from "@lexical/code";
import { ListItemNode, ListNode } from "@lexical/list";
import { registerRichText, HeadingNode, QuoteNode } from "@lexical/rich-text";
import { TableCellNode, TableNode, TableRowNode } from "@lexical/table";
import {
	$convertFromMarkdownString,
	$convertToMarkdownString,
	registerMarkdownShortcuts,
} from "@lexical/markdown";
import { TRANSFORMERS } from "./markdownTransformers";

export const bindLexical = (
	contentEditableElement: HTMLElement,
	opts?: CreateEditorArgs
) => {
	if (
		contentEditableElement == null ||
		!(contentEditableElement instanceof HTMLElement)
	) {
		throw new Error(`Element ${contentEditableElement} is invalid`);
	}
	const editable = contentEditableElement.isContentEditable;
	const args: CreateEditorArgs = {
		editable,
		...opts,
		nodes: [
			HeadingNode,
			ListNode,
			ListItemNode,
			QuoteNode,
			CodeNode,
			CodeHighlightNode,
			LinkNode,
			TableCellNode,
			TableNode,
			TableRowNode,
		],
	};
	const editor = createEditor(args);
	editor.setRootElement(contentEditableElement);
	registerRichText(editor);
	registerMarkdownShortcuts(editor, TRANSFORMERS);
	registerCodeHighlighting(editor);

	return editor;
};

export const toggleRenderedMarkdown = (editor: LexicalEditor) =>
	editor.update(() => {
		const root = $getRoot();
		const firstChild = root.getFirstChild();
		if ($isCodeNode(firstChild) && firstChild.getLanguage() === "markdown") {
			$convertFromMarkdownString(firstChild.getTextContent(), TRANSFORMERS);
		} else {
			const markdown = $convertToMarkdownString(TRANSFORMERS);
			root
				.clear()
				.append($createCodeNode("markdown").append($createTextNode(markdown)));
		}
		root.selectEnd();
	});
