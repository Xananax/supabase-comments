/**
 * A promise that can be resolved later
 */

export const deferredPromise = <T = void>() => {
	const bag = {} as {
		resolve: (value: T | PromiseLike<T>) => void;
		reject: (error: any) => void;
	};
	return Object.assign(
		new Promise<T>((resolve, reject) =>
			Object.assign(bag, { resolve, reject })
		),
		bag
	);
};
