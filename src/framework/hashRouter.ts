import { cleanUrlStr } from "./cleanUrlStr";
import { Router } from "./Router";

/**
 * @returns The current browser hash
 */
const getHash =
	typeof window !== "undefined"
		? () => window.location.hash.slice(1)
		: () => "";

/**
 * @returns the current browser hash, but only if it starts with a slash.
 * Splits the string
 */
const getHashUrl = () =>
	getHash()[0] === "/" ? cleanUrlStr(getHash()).split("/") : null;

/**
 * A client router that uses the hash as a mechanism for routing
 */
export const hashRouter = (() => {
	const router = Router();

	window.addEventListener("hashchange", (_evt) => {
		let fragment = getHashUrl();
		if (fragment != null) {
			router.set(fragment);
		}
	});

	const sync = () => {
		router.set(getHashUrl() ?? []);
	};
	return { ...router, sync };
})();
