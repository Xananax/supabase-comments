/**
 * Wrapper around querySelector which throws if element isn't found.
 * Does not return simple `Element` either, the returned element has to be an HTMLElement
 * @param this An element
 * @param selectors
 */
export function queryElement<K extends keyof HTMLElementTagNameMap>(
	this: Element,
	selectors: K
): HTMLElementTagNameMap[K];
export function queryElement<K extends keyof SVGElementTagNameMap>(
	this: Element,
	selectors: K
): SVGElementTagNameMap[K];
export function queryElement<K extends keyof MathMLElementTagNameMap>(
	this: Element,
	selectors: K
): MathMLElementTagNameMap[K];
export function queryElement<E extends HTMLElement = HTMLElement>(
	this: Element,
	selectors: string
): E;
export function queryElement(this: Element, selector: string) {
	const element = this.querySelector(selector);
	if (element == null) {
		throw new Error("Element ${selector} not found");
	}
	if (
		!(element instanceof HTMLElement) &&
		!(element instanceof SVGElement) &&
		!(element instanceof MathMLElement)
	) {
		throw new Error(`Element ${selector} is not a valid HTMLElement`);
	}
}
