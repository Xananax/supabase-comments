const templatesCache: Record<string, HTMLTemplateElement> = {};

/**
 * Returns a template element
 */
export const makeTemplate = (templateString: string) => {
	if (templateString in templatesCache) {
		return templatesCache[templateString];
	}
	const template = document.createElement("template");
	template.innerHTML = templateString;
	return template;
};

/**
 * Convenience literal to create DOM template elements
 * in VSCode, you can use [lit-html](https://marketplace.visualstudio.com/items?itemName=bierner.lit-html)
 * You should import this function and rename it to `html` for the plugin to pick it up, or edit its settings
 * and add `tmpl` to it.
 */
export const tmpl = (
	strings: TemplateStringsArray,
	...substitutions: any[]
) => {
	const formattedString = strings.reduce(
		(acc, curr, index) => acc + curr + (substitutions[index] || ""),
		""
	);
	return makeTemplate(formattedString);
};

/**
 * Creates a set of nodes from text.
 * in VSCode, you can use [lit-html](https://marketplace.visualstudio.com/items?itemName=bierner.lit-html)
 */
export const html = (strings: TemplateStringsArray, ...substitutions: any[]) =>
	tmpl(strings, ...substitutions).childNodes;
