import { cleanUrlStr } from "./cleanUrlStr";

/**
 * Transforms a string into a matching pattern.
 * Used specifically for urls, assumes `/` is a separator.
 *
 * Some characters are treated specially:
 *
 * Initial and final slash are not supposed to be in the string.
 *
 * - `*`: matches any one element of the url
 * - `**`: matches any amount of elements
 * - `:keyword`: matches any one element and puts it in the `keyword` named group
 * - `:keyword?`: same, but optionally
 * @param str
 */
export const matcherStringToRegExp = (str: string) => {
	const regexExpr =
		"^" +
		cleanUrlStr(str)
			.split("/")
			.map((str, index) => {
				const prefix = index === 0 ? "" : "\\/";
				return str === "*"
					? `${prefix}([^/]*?)`
					: str == "**"
					? `${prefix}(.*)`
					: str.startsWith(":")
					? str.endsWith("?")
						? `(?:${prefix}(?<${str.slice(1, str.length - 1)}>.[^\/]+))?`
						: `${prefix}(?<${str.slice(1)}>[^\/]+)`
					: `${prefix}${str}`;
			})
			.join("") +
		"$";
	const regex = new RegExp(regexExpr, "i");
	return regex;
};
