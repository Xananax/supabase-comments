// @ts-check
/**
 * A one file set of small tools for writing native custom elements.
 * Mostly allows proper strong typing, and does very little magic.
 *
 * What it does bundle as additional APIs:
 * - `_requestUpdate`/`update()`: batched updates
 * - `shadow` a getter for shadowDom which auto-attaches and mounts a template if there is one.
 */

import { CustomElement, type CustomElementName } from "./CustomElement";

/**
 * Builds up a strongly-typed Custom Element.
 * If your editor or tooling doesn't support Typescript, this doesn't really do much,
 * and you may use {@see CustomElement} directly.
 *
 * The only API I could find that works around Typescript's shortcomings.
 * A declarative API couldn't be incomplete, and forced to pass Events
 * as either an actual (unused) object, or do weird
 * ```
 * {
 * 	events: {} as MyElementEvents
 * }
 * ```
 * tricks.
 */
class CustomElementSpecBuilder<
	NAME extends CustomElementName,
	Events extends Record<string, any>,
	ObservedAttributes extends readonly string[]
> {
	_id: NAME | null = null;
	_events: Events | null = null;
	_template: HTMLTemplateElement | null = null;
	_observedAttributes: ObservedAttributes | null = null;

	/**
	 * Names the custom element. Names need to have a dash
	 * @param newName
	 * @returns
	 */
	name<T extends NAME>(newName: T) {
		if (newName.indexOf("-") === -1) {
			throw new Error("Template name needs to have a dash");
		}
		this._id = newName;
		return this as unknown as CustomElementSpecBuilder<
			T,
			Events,
			ObservedAttributes
		>;
	}

	/**
	 * Specifies observed attributes
	 * @param newAttributes as many strings as you want
	 * @returns
	 */
	observedAttributes<O extends ObservedAttributes>(...newAttributes: O) {
		this._observedAttributes = newAttributes;
		return this as unknown as CustomElementSpecBuilder<NAME, Events, O>;
	}

	/**
	 * Fake function that exists only to satisfy Typescript.
	 * Use it like so:
	 * ```ts
	 * WebComponent.events<{'my-event': number}>().Class
	 * ```
	 * To specify types of events. The argument is ignored, but if you prerfer passing a concrete object, you can also:
	 * ```ts
	 * WebComponent.events({'my-event': 0}).Class
	 * ```
	 * @param newEvents
	 * @returns
	 */
	events<E extends Events>(newEvents?: E) {
		if (newEvents != null) {
			this._events = newEvents;
		}
		return this as unknown as CustomElementSpecBuilder<
			NAME,
			E,
			ObservedAttributes
		>;
	}

	/**
	 * A template element. Not mandatory.
	 * @param newTemplate
	 * @returns
	 */
	template(newTemplate: HTMLTemplateElement) {
		if (!(newTemplate instanceof HTMLTemplateElement)) {
			throw new Error(`template is not a template element`);
		}
		this._template = newTemplate;
		return this as unknown as CustomElementSpecBuilder<
			NAME,
			Events,
			ObservedAttributes
		>;
	}

	/**
	 * Internal utility to create a class name; used for debugging purposes.
	 */
	get _displayName() {
		return this._id
			? this._id
					.split("-")
					.map((str) => str[0].toUpperCase() + str.slice(1))
					.join("")
			: "CustomElement";
	}

	/**
	 * Returns a strongly typed WebComponent-derived class
	 */
	get Class() {
		const {
			_id,
			_template: template,
			_observedAttributes: observedAttributes,
			_displayName: displayName,
		} = this;

		if (_id == null) {
			throw new Error(`Specification incomplete. We need an id at the minimum`);
		}

		const is = _id;

		const { [displayName]: GeneratedClass } = {
			[displayName]: class WebComponent extends CustomElement<
				NAME,
				Events,
				ObservedAttributes
			> {
				static is = is;
				static displayName = displayName;
				static template = template;
				static observedAttributes = observedAttributes ?? [];
			},
		};
		return GeneratedClass;
	}
}

// We could just change the constructor but it isn't very typescript-y
/**
 * Convenience function for creating web components without `new`.
 * @param componentName the name of the web component
 */
export const WebComponent = <
	TAG extends CustomElementName,
	Events extends Record<string, any>,
	ObservedAttributes extends readonly string[]
>(
	componentName: TAG
) => {
	const constructor = new CustomElementSpecBuilder<
		TAG,
		Events,
		ObservedAttributes
	>();
	constructor.name(componentName);
	return constructor;
};
