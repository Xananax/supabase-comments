/**
 * A convenience function to control adding an element from the child, rather than from the parent
 * @param this An Element
 * @param parent A parent element
 * @param slot Optionally, a slot to target
 * @returns the element itself
 */
export function appendTo<T extends Element>(
	this: T,
	parent: HTMLElement,
	slot?: string
) {
	if (slot) {
		this.setAttribute("slot", slot);
	}
	parent.appendChild(this);
	return this;
}
