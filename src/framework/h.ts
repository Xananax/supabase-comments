import { createElement as h, elementsFactory } from "./createElement";

export const { h1, h2, h3, a, div, ul, li, span, button, form } =
	elementsFactory(
		"h1",
		"h2",
		"h3",
		"a",
		"div",
		"ul",
		"li",
		"span",
		"button",
		"form"
	);

export { h };
