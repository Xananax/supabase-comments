// @ts-check
/**
 * convenient logger
 */
export const makeLog = (title: string) =>
	Object.assign(
		(item: any, ...items: any[]) => {
			console.log(title, item, ...items);
			return item;
		},
		{
			child(childTitle: string) {
				return makeLog(`${title}:${childTitle}`);
			},
		}
	);
