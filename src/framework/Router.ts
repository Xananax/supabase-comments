import { matcherStringToRegExp } from "./matcherStringToRegExp";

/**
 * A function that triggers when a url matches
 */
interface MatchListener<T> {
	(
		current: string[],
		data: T & {
			groups: Record<string, string> | undefined;
			matches: string[];
		}
	): void;
}

/**
 * A dispatcher that runs when the passed url matches any of the given patterns
 * @returns
 */
const matchDispatcher = <T extends Record<string, any> | void = void>() => {
	const listeners = new Map<string, [RegExp, Set<MatchListener<T>>]>();

	const on = (str: string, fn: MatchListener<T>) => {
		const collection = listeners.get(str);
		if (!collection) {
			const regexp = matcherStringToRegExp(str);
			listeners.set(str, [regexp, new Set([fn])]);
		} else {
			collection[1].add(fn);
		}
	};

	const off = (str: string, fn: MatchListener<any>) => {
		const collection = listeners.get(str);
		if (collection) {
			collection[1].delete(fn);
		}
	};

	const match = (urlArray: string[], data: T) => {
		const url = urlArray.join("/");
		listeners.forEach(([reg, fns]) => {
			const m = url.match(reg);
			if (m != null) {
				const [, ...matches] = m;
				const groups = m.groups;
				fns.forEach((l) => l(urlArray, { ...data, groups, matches }));
			}
		});
	};

	return { on, off, match };
};

/**
 * A state machine that keeps the current URL and dispatches URL changes to all listeners.
 * This is a controlled state machine, doesn't do anything on its own.
 * Pair it with a mechanism to read the hash to get a client router, for example.
 * @returns
 */
export const Router = () => {
	let url: string[] = [];
	const listenersOnEnter = matchDispatcher<{ old: string[] }>();
	const listenersOnExit = matchDispatcher<{ current: string[] }>();

	const onEnter = listenersOnEnter.on;
	const offEnter = listenersOnEnter.off;
	const onExit = listenersOnExit.on;
	const offExit = listenersOnExit.off;
	const get = () => url;
	const set = (fragment: string[]) => {
		const old = url;
		url = fragment;
		listenersOnEnter.match(url, { old });
		listenersOnExit.match(old, { current: url });
	};
	return { onEnter, offEnter, onExit, offExit, get, set };
};
