import { formatString } from "./formatString";

/**
 * Sets the page title
 * @param mainTitle The title part to always have in the page's title. Generally, the website's name
 * @param template uses the current document title by default. Use `{0}` and `{1}` to slot in the title and mainTitle respectively
 */
export const pageTitle =
	typeof document != null
		? (mainTitle = document.title, template = `{0} | {1}`) =>
				(title?: string) =>
					(document.title = title
						? formatString(template, [title, mainTitle])
						: mainTitle)
		: (mainTitle = "", template = `{0} | {1}`) =>
				(title?: string) =>
					title ? formatString(template, [title, mainTitle]) : mainTitle;
