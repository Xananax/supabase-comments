/**
 * Uses element classes as a state mechanism. Use it to synchronize state and styles.
 * @param stateName
 * @param element
 * @returns
 */
export const makeElementState = (
	stateName: string,
	element = document.body
) => {
	const on = () => element.classList.add(stateName);
	const off = () => element.classList.remove(stateName);
	const set = (value: boolean) => (value ? on() : off());
	const has = () => element.classList.contains(stateName);
	const toggle = () => set(!has());
	return { on, off, has, set, toggle };
};

/**
 * Ensures only one of the passed classes is set on the element at any one time.
 * Automatically sets the first state of the passed array.
 * @param stateNames
 * @param element
 * @returns
 */
export const makeExclusiveStates = <K extends string>(
	stateNames: K[],
	element = document.body
) => {
	const recipient = Object.fromEntries(
		stateNames.map((stateName) => [
			stateName,
			makeElementState(stateName, element),
		])
	);

	let _current: K = stateNames[0];
	const set = (state: K) => {
		if (state === _current) {
			return;
		}
		stateNames.forEach((k) => recipient[k].off());
		_current = state;
		recipient[state].on();
	};
	const get = () => _current;

	recipient[_current].on();

	return { set, get };
};
