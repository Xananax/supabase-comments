import { deferredPromise } from "./deferredPromise";
import { queryElement } from "./queryElement";

/**
 * Represents an event handler for custom events
 */
interface CustomEventListener<T> {
	(evt: CustomEvent<T>): void;
}

/**
 * Represents an event handler for custom events
 */
interface CustomEventListenerObject<T> {
	handleEvent(object: CustomEvent<T>): void;
}

/**
 * Represents an event handler for custom events
 */
type CustomEventListenerOrEventListenerObject<T> =
	| CustomEventListener<T>
	| CustomEventListenerObject<T>;

/**
 * represents a custom element type
 */
export type CustomElementName = Exclude<
	`${string}-${string}`,
	| "annotation-xml"
	| "color-profile"
	| "font-face"
	| "font-face-src"
	| "font-face-uri"
	| "font-face-format"
	| "font-face-name"
	| "missing-glyph"
>;
export class CustomElement<
	TAG extends CustomElementName,
	Events extends Record<string, any>,
	ObservedAttributes extends readonly string[]
> extends HTMLElement {
	/**
	 * For debugging purposes.
	 */
	static displayName: string;
	/**
	 * Holds the template. If set, using the `shadow` will automatically mount it
	 */
	static template: HTMLTemplateElement | null = null;
	/**
	 * Holds the custom element tag. Is useful if you want to do `element.is(MyElement.is)`
	 */
	static is: CustomElementName;
	/**
	 * Required by the API to call attributeChangedCallback
	 */
	static observedAttributes: readonly string[] | [] = [];

	/**
	 * Registers an element globally. Returns a promise that resolves when the element is available.
	 * @param options
	 * @returns
	 */
	static define(options?: ElementDefinitionOptions) {
		const tag = this.is;
		customElements.define(tag, this, options);
		return customElements.whenDefined(tag);
	}

	_hasRequestedUpdate = false;
	updateComplete = deferredPromise();

	/**
	 * Batched update handler. Only runs `update` after all microtasks have gone through.
	 * Can be called multiple times in a same frame
	 */
	async _requestUpdate() {
		if (!this._hasRequestedUpdate) {
			this._hasRequestedUpdate = true;
			this._hasRequestedUpdate = await false;
			this.update();
			this.updateComplete.resolve();
			this.updateComplete = deferredPromise();
		}
	}

	/**
	 * Runs once per frame if `_requestUpdate()` has been called at least once
	 */
	update() {}

	queryElement = queryElement;

	/**
	 * A helper function to dispatch custom events
	 * @param type the name of the event
	 * @param detail Any useful data for the event
	 */
	dispatchCustomEvent<K extends keyof Events & string, D extends Events[K]>(
		type: K,
		detail: D
	) {
		this.dispatchEvent(
			new CustomEvent(type, {
				bubbles: true,
				cancelable: false,
				composed: true,
				detail,
			})
		);
	}

	/**
	 * The only reason we override this is so we can strongly type it.
	 *
	 * Appends an event listener for events whose type attribute value is type. The callback argument sets the callback that will be invoked when the event is dispatched.
	 *
	 * The options argument sets listener-specific options. For compatibility this can be a boolean, in which case the method behaves exactly as if the value was specified as options's capture.
	 *
	 * When set to true, options's capture prevents callback from being invoked when the event's eventPhase attribute value is BUBBLING_PHASE. When false (or not present), callback will not be invoked when event's eventPhase attribute value is CAPTURING_PHASE. Either way, callback will be invoked if event's eventPhase attribute value is AT_TARGET.
	 *
	 * When set to true, options's passive indicates that the callback will not cancel the event by invoking preventDefault(). This is used to enable performance optimizations described in § 2.8 Observing event listeners.
	 *
	 * When set to true, options's once indicates that the callback will only be invoked once after which the event listener will be removed.
	 *
	 * If an AbortSignal is passed for options's signal, then the event listener will be removed when signal is aborted.
	 *
	 * The event listener is appended to target's event listener list and is not appended if it has the same type, callback, and capture.
	 *
	 * MDN Reference Appends an event listener for events whose type attribute value is type. The callback argument sets the callback that will be invoked when the event is dispatched.
	 *
	 * The options argument sets listener-specific options. For compatibility this can be a boolean, in which case the method behaves exactly as if the value was specified as options's capture.
	 *
	 * When set to true, options's capture prevents callback from being invoked when the event's eventPhase attribute value is BUBBLING_PHASE. When false (or not present), callback will not be invoked when event's eventPhase attribute value is CAPTURING_PHASE. Either way, callback will be invoked if event's eventPhase attribute value is AT_TARGET.
	 *
	 * When set to true, options's passive indicates that the callback will not cancel the event by invoking preventDefault(). This is used to enable performance optimizations described in § 2.8 Observing event listeners.
	 *
	 * When set to true, options's once indicates that the callback will only be invoked once after which the event listener will be removed.
	 *
	 * If an AbortSignal is passed for options's signal, then the event listener will be removed when signal is aborted.
	 *
	 * The event listener is appended to target's event listener list and is not appended if it has the same type, callback, and capture.
	 */
	public addEventListener<T extends keyof Events>(
		type: T,
		listener: CustomEventListenerOrEventListenerObject<Events[T]>,
		options?: boolean | AddEventListenerOptions
	): void {
		// @ts-ignore
		super.addEventListener(type, listener, options);
	}

	/**
	 * Simple helper setter to add or remove a boolean attribute
	 * @param name name of the attribute
	 * @param val The value
	 */
	setBooleanAttribute(
		name: string,
		val: boolean | string | undefined | number | null
	) {
		if (
			(typeof val === "string" &&
				(val === "" ||
					val === "true" ||
					val === "on" ||
					val.toLowerCase() === name.toLowerCase())) ||
			!!val
		) {
			this.setAttribute(name, "");
		} else {
			this.removeAttribute(name);
		}
	}

	/**
	 * Removes all slotted elements
	 */
	clearLightDom() {
		while (this.firstChild) {
			this.removeChild(this.lastChild!);
		}
	}

	/**
	 * Lazily initializes the shadow root when requested the first time.
	 * Also clones and attaches the static template if there is one.
	 */
	get shadow(): ShadowRoot {
		this._instantiateTemplate();
		return this.shadowRoot!;
	}

	_instantiateTemplate() {
		if (!this.shadowRoot) {
			this.attachShadow({ mode: "open" });
			const constructor = <typeof CustomElement>this.constructor;
			if (constructor.template instanceof HTMLTemplateElement) {
				this.shadowRoot!.append(constructor.template.content.cloneNode(true));
				// this.shadowRoot!.queryElement = queryElement
			}
		}
	}

	/**
	 * Called anytime one of the elements referenced in observedAttributes
	 * @param name the name of the attribute that was changed
	 * @param oldValue the previous value
	 * @param newValue the next value
	 */
	attributeChangedCallback(
		name: ObservedAttributes[number],
		oldValue: string,
		newValue: string
	) {}
}
