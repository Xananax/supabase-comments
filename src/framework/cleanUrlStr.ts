/**
 * Removes initial and final slashes, as well as repeat slashes
 */
export const cleanUrlStr = (str: string) =>
	str.replace(/\/\/+/g, "/").replace(/^\/|\/$/g, "");
