/**
 * Verifies a switch statement is complete/exhaustive by sticking this in `default:` or after the switch
 * @param _some the variable you're switching on
 */
export function assertUnreachable(_some: never): never {
	throw new Error("A switch case was not handled");
}
