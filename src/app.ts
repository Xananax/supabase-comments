import type { PostgrestError, Session, User } from "@supabase/supabase-js";
import {
	Signal,
	Collection,
	hashRouter,
	makeLog,
	pageTitle,
	makeExclusiveStates,
	makeElementState,
	button,
	span,
	bindSignalToNode as bind,
} from "./framework";
import { elementsAreReady } from "./elements";
import {
	getSession,
	getPostsListBySlug,
	createThread,
	createPost,
	getAllPostsPages,
	updatePost,
	deletePost,
	votePost,
	pinPost,
} from "./api";
import { createTicket, getAllTickets } from "./api/tickets";
import { TextNode } from "lexical";

const reload = () => location.reload();

/******************************** Globals ********************************/

const user = Signal<User | null>(null, {
	transform: (user, old, SKIP) =>
		user != null && old != null && user.id === old.id ? SKIP : user,
});

const session = Signal<Session | null>(null).on((newSession) =>
	user.set(
		newSession == null || newSession.user == null ? null : newSession.user
	)
);

const userMessagesCollection = Collection<Error | PostgrestError | string>();

/******************************** Bootstrapping ********************************/

const bootstrap = async () => {
	const log = makeLog("boot");

	const changeTitle = pageTitle();

	/******************************** handle errors ********************************/

	/**
	 * Holds all messages to the user
	 */
	const messagesList = document.querySelector("messages-list")!;

	/**
	 * Make sure to append a message to the dom every time it appears in the list
	 */
	userMessagesCollection.on((messages) => (messagesList.data = messages));

	/** Remove a message when closed */
	messagesList.addEventListener("close-message", ({ detail: { index } }) =>
		userMessagesCollection.removeAt(index)
	);

	/******************************** handle auth **********************************/

	const bodyLoggedInState = makeExclusiveStates(["loggedout", "loggedin"]);
	const bodyModerationState = makeElementState("is_moderator");
	const userForm = document.querySelector("user-form")!;
	userForm.loggedin = false;
	user.on((value) => {
		const isModerator = value?.app_metadata?.userrole === "MODERATOR" ?? false;
		threadList.userId = value?.id;
		threadList.moderationMode = isModerator;
		bodyModerationState.set(isModerator);
		if (value) {
			bodyLoggedInState.set("loggedin");
			userForm.loggedin = true;
		} else {
			bodyLoggedInState.set("loggedout");
			userForm.loggedin = false;
		}
	});

	const auth = getSession({
		onSessionChanged: session.set,
		onError: userMessagesCollection.append,
	});

	userForm.addEventListener("login-attempt", ({ detail: { email } }) => {
		session.set(null);
		userForm.disabled = true;
		auth.loginOTP(email).then(() => {
			userForm.disabled = false;
			userMessagesCollection.append("please check your email to log in");
		});
	});
	userForm.addEventListener("logout-attempt", auth.logout);

	/******************************** handle posting **********************************/

	const threadForm = document.querySelector("thread-form")!;
	const threadFormDialog = document.getElementById(
		"ThreadFormDialog"
	) as HTMLDialogElement;

	threadForm &&
		threadForm.addEventListener("submit", ({ detail }) => {
			switch (detail.mode) {
				case "create": {
					const { mode: _mode, ...data } = detail;
					return createThread(data)
						.then((data) => console.log(data))
						.then(reload)
						.catch(userMessagesCollection.append);
				}
				case "reply": {
					const { mode: _mode, ...data } = detail;
					return createPost(data)
						.then((data) => console.log(data))
						.then(reload)
						.catch(userMessagesCollection.append);
				}
				case "update": {
					const { mode: _mode, ...data } = detail;
					return updatePost(data)
						.then((data) => console.log(data))
						.then(reload)
						.catch(userMessagesCollection.append);
				}
				case "delete": {
					const { post_id } = detail;
					return deletePost(post_id)
						.then((data) => console.log(data))
						.then(reload)
						.catch(userMessagesCollection.append);
				}
			}
		});

	document.querySelectorAll("button.new-post").forEach((btn) => {
		btn.addEventListener("click", () => {
			threadForm.mode = "create";
			threadForm.title = "";
			threadForm.hideTitle = false;
			threadFormDialog.showModal();
		});
	});

	/******************************** handle contact form **********************************/

	const contactForm = document.querySelector("contact-form");
	contactForm?.addEventListener("submit", ({ detail }) => {
		createTicket(detail)
			.then(() =>
				userMessagesCollection.append("Thank you! We will contact you back")
			)
			.catch(userMessagesCollection.append);
	});

	/******************************** handle threads **********************************/
	const threadList = document.querySelector("thread-post-list")!;
	threadList.addEventListener(
		"reply-request",
		({ detail: { post_id, title } }) => {
			threadForm.mode = "reply";
			threadForm.replyTo = post_id;
			threadForm.title = title;
			threadForm.hideTitle = true;
			threadFormDialog.showModal();
		}
	);
	threadList.addEventListener(
		"edit-request",
		({ detail: { post_id, title, body_raw, reply_to_id } }) => {
			threadForm.mode = "update";
			threadForm.postId = post_id;
			threadForm.title = title;
			threadForm.hideTitle = true;
			threadForm.replyTo = reply_to_id;
			threadForm.$richTextEditor.value = body_raw;
			threadFormDialog.showModal();
		}
	);
	threadList.addEventListener(
		"vote-request",
		({ detail: { post_id, vote_value } }) => {
			votePost(post_id, vote_value).then(reload);
		}
	);
	threadList.addEventListener("delete-request", ({ detail: { post_id } }) => {
		deletePost(post_id).then(reload);
	});

	threadList.addEventListener(
		"pin-request",
		({ detail: { post_id, pin_value } }) => {
			pinPost(post_id, pin_value).then(reload);
		}
	);

	threadList.addEventListener(
		"hide-request",
		({ detail: { post_id, hide_value } }) => {
			pinPost(post_id, hide_value).then(reload);
		}
	);

	/******************************** pagination **********************************/

	const pageState = makeExclusiveStates([
		"page-home",
		"page-urls",
		"page-posts",
		"page-profile",
		"page-contact",
		"page-admin",
	]);

	hashRouter.onEnter("**", ([firstSegment, secondSegment]) => {
		switch (firstSegment) {
			case "posts":
				if (secondSegment) {
					pageState.set("page-posts");
				} else {
					pageState.set("page-urls");
				}
				break;
			case "profile":
			case "contact":
			case "admin":
				pageState.set(`page-${firstSegment}`);
				break;
			default:
				pageState.set("page-home");
				break;
		}
	});

	const postsUrlsList = document.getElementById("PostsUrlsList")!;
	const ticketsList = document.getElementById("TicketsList")!;

	// we could load this on page load and only invalidate/refresh if something new is posted, but this is simpler
	hashRouter.onEnter("posts", () => {
		getAllPostsPages()
			.then(
				(slugs) =>
					(postsUrlsList.innerHTML = slugs
						.map(
							({ post_slug }) =>
								`<li><a href="#/posts${post_slug}">${post_slug}</a></li>`
						)
						.join(""))
			)
			.catch(userMessagesCollection.append);
	});

	// we could load this on page load and only invalidate/refresh if something new is posted, but this is simpler
	hashRouter.onEnter("admin", () => {
		getAllTickets()
			.then(
				(tickets) =>
					(ticketsList.innerHTML = tickets
						.map(
							({ body_raw, created_at, flagged_reason, status, user_email }) =>
								`<li><pre>${JSON.stringify(
									{ body_raw, created_at, flagged_reason, status, user_email },
									null,
									2
								)}</pre></li>`
						)
						.join(""))
			)
			.catch(userMessagesCollection.append);
	});

	hashRouter.onEnter("posts/**", ([, ...urlParts]) => {
		const url = `/${urlParts.join("/")}`;
		threadForm && (threadForm.url = url);
		getPostsListBySlug(url)
			.then((data) => {
				changeTitle(`${urlParts[1]} Posts`);
				if (data == null || data.length === 0) {
					threadList.empty = true;
					threadList.data = null;
					return;
				}
				threadList.empty = false;
				threadList.data = data;
			})
			.catch(userMessagesCollection.append);
	});
};

/**
 * This is unrelated to anything supabase, I just realized I made a reactive framework so this is an example of a component
 * @returns
 */
const Counter = () => {
	const randFromArr =
		<K>(arr: K[]) =>
		() =>
			arr[Math.floor(Math.random() * arr.length)];
	const randomColor = randFromArr([
		"#cdb4db",
		"#ffc8dd",
		"#ffafcc",
		"#bde0fe",
		"#a2d2ff",
	]);
	const counter = Signal(0);
	const color = Signal(randomColor());
	counter.on((v) => console.log(v));
	counter.on(() => color.set(randomColor()));
	color.on((v) => console.log(v));
	return span(
		{
			style: bind(
				color,
				(v) =>
					`background-color:${v}; display:inline-block; padding:2em; text-align:center;`
			),
		},
		" ",
		bind(counter, (v) => v + ""),
		" ",
		button({ onclick: () => counter.set(counter.get() + 1) }, "👍"),
		button({ onclick: () => counter.set(counter.get() - 1) }, "👎")
	);
};

elementsAreReady
	.then(bootstrap)
	.then(hashRouter.sync)
	.then(() => {
		const footer = document.querySelector("footer");
		footer && Counter().appendTo(footer);
		footer && Counter().appendTo(footer);
	});
