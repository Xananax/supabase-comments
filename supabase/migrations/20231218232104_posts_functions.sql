DROP FUNCTION IF EXISTS public.create_thread;
CREATE OR REPLACE FUNCTION public.create_thread(
  title       TEXT,
  post_slug   TEXT,
  body_raw    TEXT,
  --post_tags   BIGINT[] DEFAULT '{}',
  post_tags   TEXT[] DEFAULT '{}',
  user_id     UUID     DEFAULT auth.uid()
) RETURNS public.posts
AS $$
DECLARE
  new_post public.posts;
BEGIN

  INSERT INTO public.posts (
    user_id,
    title,
    post_slug,
    body_raw,
    post_tags
  )
  VALUES (
    user_id,
    title,
    post_slug,
    body_raw,
    (SELECT array_agg(tag_id) FROM public.tags WHERE tag_slug = ANY(post_tags))
  )
  RETURNING * INTO new_post;
  RETURN new_post;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.create_thread IS 'Creates a thread (a post that replies to nothing)';


DROP FUNCTION IF EXISTS public.create_post;
CREATE OR REPLACE FUNCTION public.create_post(
  body_raw    TEXT,
  reply_to_id BIGINT,
	user_id     UUID  DEFAULT auth.uid()
) RETURNS public.posts
AS $$
DECLARE
  parent_post public.posts;
  new_post    public.posts;
BEGIN
  -- Fetch information about the post being replied to
  SELECT * INTO parent_post
  FROM public.posts
  WHERE post_id = create_post.reply_to_id;

  -- Check if the parent post exists
  IF NOT FOUND THEN
    RAISE EXCEPTION 'Parent post with ID % not found.', create_post.reply_to_id;
  END IF;

  -- Create the new post
  INSERT INTO public.posts (
    user_id,
    body_raw,
    title,
    post_tags,
    reply_to_id,
    post_slug
  )
  VALUES (
    create_post.user_id,
    create_post.body_raw,
    parent_post.title,
    parent_post.post_tags,
    create_post.reply_to_id,
    parent_post.post_slug
  )
  RETURNING * INTO new_post;

  RETURN new_post;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.create_post IS 'Creates a post (a reply to a thread)';

DROP FUNCTION IF EXISTS public.delete_post;
CREATE OR REPLACE FUNCTION public.delete_post(
	post_id BIGINT
) RETURNS void
AS
$$
  UPDATE
  	posts
  SET 
  	deleted_at = now()
  WHERE
  	posts.post_id = delete_post.post_id
$$ LANGUAGE SQL;
COMMENT ON FUNCTION public.delete_post IS 'Sets a post as deleted';