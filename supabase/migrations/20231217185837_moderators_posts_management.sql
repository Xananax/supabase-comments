-- MODERATOR POST MANAGEMENT

-------------------------------------------------------------------------------
-- TABLES ---------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP TABLE IF EXISTS public.moderated_posts;
CREATE TABLE public.moderated_posts(
  post_id      BIGINT       NOT NULL REFERENCES public.posts(post_id) ON DELETE CASCADE,
  is_pinned    BOOLEAN      NOT NULL DEFAULT false,
  is_hidden    BOOLEAN      NOT NULL DEFAULT false,
  created_at   TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  updated_at   TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  moderator_id UUID         REFERENCES auth.users(id) ON DELETE SET NULL,
  notes        TEXT         NULL,
  PRIMARY KEY (post_id)
);

COMMENT ON TABLE  public.moderated_posts              IS 'Stores posts moderator settings';
COMMENT ON COLUMN public.moderated_posts.post_id      IS 'The post''s unique id, links to the posts table';
COMMENT ON COLUMN public.moderated_posts.is_pinned    IS 'If true, the post will be prioritary';
COMMENT ON COLUMN public.moderated_posts.is_hidden    IS 'If true, the posts will not show';
COMMENT ON COLUMN public.moderated_posts.created_at   IS 'The time of the moderator''s meta post creation';
COMMENT ON COLUMN public.moderated_posts.updated_at   IS 'The time of the moderator''s meta post creation';
COMMENT ON COLUMN public.moderated_posts.moderator_id IS 'The id of the moderator who made the meta post';
COMMENT ON COLUMN public.moderated_posts.notes        IS 'Any useful note (e.g, reasons for hiding or deleting)';

-------------------------------------------------------------------------------
-- SPECIAL RULES/INDICES ------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TRIGGER on_post_meta_update_sync_time BEFORE UPDATE ON public.moderated_posts FOR EACH ROW EXECUTE PROCEDURE sync_last_modification();
ALTER TABLE public.moderated_posts ENABLE ROW LEVEL SECURITY;

-------------------------------------------------------------------------------
-- SECURITY/RLS ---------------------------------------------------------------
-------------------------------------------------------------------------------

DROP POLICY IF EXISTS moderators_can_use_moderated_posts ON public.posts;
CREATE POLICY moderators_can_use_moderated_posts
ON public.moderated_posts FOR ALL to authenticated
USING (is_moderator() )
WITH CHECK (is_moderator() );


-------------------------------------------------------------------------------
-- FUNCTIONS ------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.pin_post;
CREATE OR REPLACE FUNCTION public.pin_post(
  post_id     BIGINT,
  pin_value   BOOLEAN,
  user_id     UUID     DEFAULT auth.uid()
) RETURNS TEXT
AS $$
BEGIN

  INSERT INTO public.moderated_posts (
    post_id,
    is_pinned,
    moderator_id
  )
  VALUES (
    post_id,
    pin_value,
    user_id
  )
  ON CONFLICT (post_id) DO
  UPDATE 
  SET is_pinned = EXCLUDED.is_pinned;
  RETURN 'OK';
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.pin_post IS 'Pins a thread -- moderators only';


DROP FUNCTION IF EXISTS public.hide_post;
CREATE OR REPLACE FUNCTION public.hide_post(
  post_id     BIGINT,
  hide_value  BOOLEAN,
  user_id     UUID     DEFAULT auth.uid()
) RETURNS TEXT
AS $$
BEGIN

  INSERT INTO public.moderated_posts (
    post_id,
    is_hidden,
    moderator_id
  )
  VALUES (
    post_id,
    hide_value,
    user_id
  )
  ON CONFLICT (post_id) DO
  UPDATE 
  SET is_hidden = EXCLUDED.is_hidden;
  RETURN 'OK';
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.pin_post IS 'Hides a thread -- moderators only';