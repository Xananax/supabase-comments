-------------------------------------------------------------------------------
-- VIEWS ----------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.get_thread_trees_by_slug;
DROP FUNCTION IF EXISTS public.get_thread_tree_by_id;
DROP VIEW IF EXISTS public.posts_with_path;

DROP VIEW IF EXISTS public.posts_with_meta;
CREATE VIEW public.posts_with_meta AS
  SELECT DISTINCT ON (post_id)
    -- posts
    posts.post_id,
    posts.user_id,
    posts.title,
    posts.post_slug,
    posts.post_tags,
    posts.created_at,
    posts.updated_at,
    posts.previous_versions,
    (CASE WHEN (posts.deleted_at IS NULL) THEN posts.body_raw ELSE '(deleted)' END) as body_raw,
    posts.is_published,
    posts.reply_to_id,
    posts.deleted_at,
    -- tags
    (
      SELECT array_agg(tags.tag_slug) 
      FROM tags 
      WHERE tags.tag_id = ANY(post_tags)
    ) AS tags_slugs,
    -- moderated posts
    COALESCE(moderated_posts.is_pinned, false) AS is_pinned,
    COALESCE(moderated_posts.is_hidden, false) AS is_hidden,
    -- profiles
    profiles.nickname,
    profiles.hide_info,
    profiles.avatar_url,
    -- generated fields: votes
    COALESCE((SELECT SUM(votes.vote_value) FROM votes WHERE votes.post_id = posts.post_id), 0) AS vote_score,
    -- generated fields: meta
    COALESCE((posts.deleted_at IS NOT NULL), false) AS is_deleted,
    COALESCE(self_votes.vote_value, 0) AS self_score,
    COALESCE((SELECT posts.user_id = auth.uid()), false) AS is_author
  FROM
    posts
    INNER JOIN profiles 
      ON posts.user_id = profiles.user_id
    LEFT JOIN moderated_posts 
      ON moderated_posts.post_id = posts.post_id
    LEFT JOIN votes
      ON votes.post_id = posts.post_id
		LEFT JOIN LATERAL (
      SELECT vote_value
      FROM votes 
      WHERE votes.user_id = auth.uid() 
      AND votes.post_id = posts.post_id 
      LIMIT 1
    ) self_votes ON TRUE
    WINDOW votes_window AS (
      PARTITION BY votes.post_id
    );
COMMENT ON VIEW public.posts_with_meta IS 'Posts with their authors, moderation status, tags, and votes. If the user is logged in, there will also be some additional information';


-- see: https://github.com/lawrencecchen/threaded-comments/blob/main/sql/tables.sql
-- This is TOO SLOW, I keep it for reference but it shouldn't be used
/*
DROP VIEW IF EXISTS public.posts_with_path;
CREATE RECURSIVE VIEW public.posts_with_path (
  post_id,
  user_id,
  title,
  post_slug,
  post_tags,
  created_at,
  updated_at,
  previous_versions,
  body_raw,
  is_published,
  reply_to_id,
  deleted_at,
  tags_slugs,
  is_pinned,
  is_hidden,
  nickname,
  hide_info,
  avatar_url,
  vote_score,
  is_deleted,
  -- generated fields
  depth,
  path,
  path_votes_or_recent,
  path_least_recent,
  path_most_recent
) AS SELECT
    post_id,
    user_id,
    title,
    post_slug,
    post_tags,
    created_at,
    updated_at,
    previous_versions,
    body_raw,
    is_published,
    reply_to_id,
    deleted_at,
    tags_slugs,
    is_pinned,
    is_hidden,
    nickname,
    hide_info,
    avatar_url,
    vote_score,
    is_deleted,
    0 as depth,
    array[post_id] as path,
    array[post_id] as path_votes_or_recent,
    array[post_id] as path_least_recent,
    array[post_id] as path_most_recent
  FROM
    posts_with_meta
  WHERE
    reply_to_id is null
  UNION SELECT
        p1.post_id,
        p1.user_id,
        p1.title,
        p1.post_slug,
        p1.post_tags,
        p1.created_at,
        p1.updated_at,
        p1.previous_versions,
        p1.body_raw,
        p1.is_published,
        p1.reply_to_id,
        p1.deleted_at,
        p1.tags_slugs,
        p1.is_pinned,
        p1.is_hidden,
        p1.nickname,
        p1.hide_info,
        p1.avatar_url,
        p1.vote_score,
        p1.is_deleted,
        p2.depth + 1 as depth,
        p2.path || p1.post_id::bigint as path,
        p2.path_votes_or_recent || -p1.vote_score::bigint || -extract(epoch from p1.created_at)::bigint || p1.post_id as path_votes_or_recent,
        p2.path_least_recent || extract(epoch from p1.created_at)::bigint || p1.post_id as path_least_recent,
        p2.path_most_recent || -extract(epoch from p1.created_at)::bigint || p1.post_id as path_most_recent
    from
        posts_with_meta p1
        join posts_with_path p2 on p1.reply_to_id = p2.post_id;
COMMENT ON VIEW public.posts_with_path IS 'Posts with 3 paths fields, one by votes or recency, one by least recent, one by most recent. There''s also a depth field';
*/

DROP VIEW IF EXISTS public.posts_slugs;
CREATE VIEW public.posts_slugs AS
  SELECT DISTINCT post_slug FROM posts;
COMMENT ON VIEW public.posts_with_meta IS 'Posts urls';



-- see: https://danielfgray.com/articles/diy-comments
-- Kept for reference, but also too slow
/**
DROP FUNCTION IF EXISTS public.get_thread_tree_by_id;
CREATE OR REPLACE FUNCTION public.get_thread_tree_by_id(
  post_id BIGINT
) RETURNS JSON AS
$$
  SELECT json_build_object(
    'post_id', c.post_id,
    'reply_to_id', c.reply_to_id,
    'title', c.title,
    'tags_ids', c.post_tags,
    'tags_slugs', c.tags_slugs,
    'slug', c.post_slug,
    'is_author', (SELECT user_id = auth.uid()),
    'author', json_build_object(
      'nickname', nickname,
      'hide_info', hide_info,
      'avatar_url', avatar_url
    ),
    'body_raw', body_raw,
    'created_at', c.created_at,
    'vote_score', vote_score,
    'self_score', (SELECT vote_value FROM votes WHERE votes.user_id = auth.uid() AND votes.post_id = c.post_id),
    'children', children
  )
  FROM
    posts_with_meta c,
  lateral (
    SELECT coalesce(
      json_agg(
        get_thread_tree_by_id(posts_with_meta.post_id)
        ORDER BY created_at ASC
        )
      , '[]') AS children
    FROM 
      posts_with_meta
    WHERE 
      posts_with_meta.reply_to_id = c.post_id
  ) AS get_children
  WHERE
    c.post_id = get_thread_tree_by_id.post_id
$$ LANGUAGE SQL STABLE;
COMMENT ON FUNCTION public.get_thread_tree_by_id IS 'Returns a JSON tree of the post for a provided id';
**/


-- Also too slow :(
/*
DROP FUNCTION IF EXISTS public.get_thread_trees_by_slug;
CREATE OR REPLACE FUNCTION public.get_thread_trees_by_slug(
  post_slug TEXT
) RETURNS TABLE (
    post_id     BIGINT,
    reply_to_id BIGINT,
    title       TEXT,
    post_tags   BIGINT[],
    tags_slugs  TEXT[],
    post_slug   TEXT,
    is_author   BOOL,
    body_raw    TEXT,
    created_at  TIMESTAMP(3),
    vote_score  BIGINT,
    self_score  BIGINT,
    children    JSON,
    author      JSON
  )
AS
$$
  SELECT 
  	c.post_id,
    c.reply_to_id,
    c.title,
    c.post_tags,
    c.tags_slugs,
    c.post_slug,
    (SELECT user_id = auth.uid()) AS is_author,
    c.body_raw,
    c.created_at,
    c.vote_score,
    (SELECT vote_value FROM votes WHERE votes.user_id = auth.uid() AND votes.post_id = c.post_id) AS self_score,
    children,
    json_build_object(
      'nickname', nickname,
      'hide_info', hide_info,
      'avatar_url', avatar_url
    ) AS author
  FROM
    posts_with_meta c,
  lateral (
    SELECT coalesce(
      json_agg(
        get_thread_tree_by_id(posts_with_meta.post_id)
        ORDER BY created_at ASC
        )
      , '[]') AS children
    FROM 
      posts_with_meta
    WHERE 
      posts_with_meta.reply_to_id = c.post_id
  ) AS get_children
  WHERE
    c.post_slug = get_thread_trees_by_slug.post_slug
    AND
    c.reply_to_id IS NULL;
$$ LANGUAGE SQL STABLE;
COMMENT ON FUNCTION public.get_thread_trees_by_slug IS 'Returns a JSON tree of the post for a provided slug';
*/

-- Unused, but left because could be useful if someone really needs deep hierarchies:
/**
-------------------------------------------------------------------------------
-- TABLES ---------------------------------------------------------------------
-------------------------------------------------------------------------------

-- see: https://www.slideshare.net/billkarwin/models-for-hierarchical-data (slide 41)
DROP TABLE IF EXISTS tree_paths;
CREATE TABLE tree_paths(
  user_id    UUID    REFERENCES auth.users(id) ON DELETE SET NULL,
	ancestor   BIGINT  REFERENCES public.posts(post_id) ON DELETE CASCADE,
  descendant BIGINT  REFERENCES public.posts(post_id) ON DELETE CASCADE,
  PRIMARY KEY (ancestor, descendant)
);

COMMENT ON TABLE  public.tree_paths            IS 'Stores posts'' relations. Each post is at least related to itself';
COMMENT ON COLUMN public.posts.user_id         IS 'The post''s user id.';
COMMENT ON COLUMN public.tree_paths.ancestor   IS 'The post''s parent unique id';
COMMENT ON COLUMN public.tree_paths.descendant IS 'The post''s children user id.';

-------------------------------------------------------------------------------
-- SECURITY/RLS ---------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.posts ENABLE ROW LEVEL SECURITY;

DROP POLICY IF EXISTS anon_can_see_posts_paths ON public.tree_paths;
CREATE POLICY anon_can_see_posts_paths
ON public.tree_paths FOR SELECT
USING ( true );

DROP POLICY IF EXISTS authenticated_can_create_new_post_paths ON public.tree_paths;
CREATE POLICY authenticated_can_create_new_post
ON public.tree_paths FOR INSERT TO authenticated
WITH CHECK ( auth.uid() = user_id );

DROP POLICY IF EXISTS mod_can_update_post_paths ON public.tree_paths;
CREATE POLICY mod_can_update_post_paths
ON public.tree_paths FOR UPDATE
USING ( is_moderator() );

-------------------------------------------------------------------------------
-- FUNCTIONS ------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.get_thread_tree_by_id;
CREATE OR REPLACE FUNCTION public.get_thread_tree_by_id(
  post_id BIGINT
) RETURNS posts_with_meta 
AS
$$
	SELECT c.* FROM posts_with_meta c
  JOIN tree_paths t
  ON c.post_id = t.descendant
  WHERE t.ancestor = 4
$$ LANGUAGE SQL STABLE;


DROP FUNCTION IF EXISTS public.create_thread;
CREATE OR REPLACE FUNCTION public.create_thread(
  title       TEXT,
  post_slug   TEXT,
  body_raw    TEXT,
  post_tags   TEXT[] DEFAULT '{}',
  user_id     UUID     DEFAULT auth.uid()
) RETURNS public.posts
AS $$
DECLARE
  new_post public.posts;
BEGIN

  INSERT INTO public.posts (
    user_id,
    title,
    post_slug,
    body_raw,
    post_tags
  )
  VALUES (
    user_id,
    title,
    post_slug,
    body_raw,
    (SELECT array_agg(tag_id) FROM public.tags WHERE tag_slug = ANY(post_tags))
  )
  RETURNING * INTO new_post;
  
  INSERT INTO tree_paths VALUES (user_id, new_post.post_id, new_post.post_id);
  RETURN new_post;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.create_thread IS 'Creates a thread (a post that replies to nothing)';


DROP FUNCTION IF EXISTS public.create_post;
CREATE OR REPLACE FUNCTION public.create_post(
  body_raw    TEXT,
  reply_to_id BIGINT,
	user_id     UUID  DEFAULT auth.uid()
) RETURNS public.posts
AS $$
DECLARE
  parent_post public.posts;
  new_post    public.posts;
BEGIN
  -- Fetch information about the post being replied to
  SELECT * INTO parent_post
  FROM public.posts
  WHERE post_id = create_post.reply_to_id;

  -- Check if the parent post exists
  IF NOT FOUND THEN
    RAISE EXCEPTION 'Parent post with ID % not found.', create_post.reply_to_id;
  END IF;

  -- Create the new post
  INSERT INTO public.posts (
    user_id,
    body_raw,
    title,
    post_tags,
    post_slug
  )
  VALUES (
    create_post.user_id,
    create_post.body_raw,
    parent_post.title,
    parent_post.post_tags,
    parent_post.post_slug
  )
  RETURNING * INTO new_post;

	INSERT INTO tree_paths (user_id, ancestor, descendant)
  SELECT user_id, ancestor, new_post.post_id
  FROM tree_paths
  WHERE descendant = reply_to_id
  UNION ALL SELECT new_post.post_id, new_post.post_id;

  RETURN new_post;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.create_post IS 'Creates a post (a reply to a thread)';
**/