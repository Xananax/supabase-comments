-------------------------------------------------------------------------------
-- UTILITY FUNCS --------------------------------------------------------------
-------------------------------------------------------------------------------

-- SCHEMA

CREATE SCHEMA IF NOT EXISTS posts_utils;
GRANT USAGE ON SCHEMA posts_utils TO anon, authenticated, service_role;
GRANT ALL ON ALL TABLES IN SCHEMA posts_utils TO anon, authenticated, service_role;
GRANT ALL ON ALL ROUTINES IN SCHEMA posts_utils TO anon, authenticated, service_role;
GRANT ALL ON ALL SEQUENCES IN SCHEMA posts_utils TO anon, authenticated, service_role;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA posts_utils GRANT ALL ON TABLES TO anon, authenticated, service_role;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA posts_utils GRANT ALL ON ROUTINES TO anon, authenticated, service_role;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA posts_utils GRANT ALL ON SEQUENCES TO anon, authenticated, service_role;

DROP TYPE IF EXISTS posts_utils.TERM_TYPE;
CREATE TYPE posts_utils.TERM_TYPE AS ENUM ('ADJECTIVE', 'ANIMAL'); -- noqa

-- thanks to: https://github.com/bryanmylee/zoo-ids

CREATE TABLE IF NOT EXISTS posts_utils.terms(
  term_type posts_utils.TERM_TYPE NOT NULL,
  term      TEXT                  NOT NULL PRIMARY KEY,
  UNIQUE(term, term_type)
);
-- inserting 1339 adjectives
-- we have to chunk it because Supabase chokes on large strings
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['abandoned', 'able', 'absolute', 'adventurous', 'academic', 'acceptable', 'acclaimed', 'accomplished', 'accurate', 'aching', 'acidic', 'acrobatic', 'active', 'actual', 'adept', 'admirable', 'admired', 'adolescent', 'adorable', 'adored', 'advanced', 'afraid']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['affectionate', 'aged', 'aggravating', 'aggressive', 'agile', 'agitated', 'agonizing', 'agreeable', 'ajar', 'alarmed', 'alarming', 'alert', 'alienated', 'alive', 'all', 'altruistic', 'amazing', 'ambitious', 'ample', 'amused', 'amusing', 'anchored', 'ancient']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['angelic', 'angry', 'anguished', 'animated', 'annual', 'another', 'antique', 'anxious', 'any', 'apprehensive', 'appropriate', 'apt', 'arctic', 'arid', 'aromatic', 'artistic', 'ashamed', 'assured', 'astonishing', 'athletic', 'attached', 'attentive', 'attractive']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['austere', 'authentic', 'authorized', 'automatic', 'avaricious', 'average', 'aware', 'awesome', 'awful', 'awkward', 'babyish', 'bad', 'back', 'baggy', 'bare', 'barren', 'basic', 'beautiful', 'belated', 'beloved', 'beneficial', 'better', 'best', 'bewitched', 'big']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['big-hearted', 'biodegradable', 'bite-sized', 'bitter', 'black', 'black-and-white', 'bland', 'blank', 'blaring', 'bleak', 'blind', 'blissful', 'blond', 'blue', 'blushing', 'bogus', 'boiling', 'bold', 'bony', 'boring', 'bossy', 'both', 'bouncy', 'bountiful', 'bowed']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['brave', 'breakable', 'brief', 'bright', 'brilliant', 'brisk', 'broken', 'bronze', 'brown', 'bruised', 'bubbly', 'bulky', 'bumpy', 'buoyant', 'burdensome', 'burly', 'bustling', 'busy', 'buttery', 'buzzing', 'calculating', 'calm', 'candid', 'canine', 'capital']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['carefree', 'careful', 'careless', 'caring', 'cautious', 'cavernous', 'celebrated', 'charming', 'cheap', 'cheerful', 'cheery', 'chief', 'chilly', 'chubby', 'circular', 'classic', 'clean', 'clear', 'clear-cut', 'clever', 'close', 'closed', 'cloudy', 'clueless']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['clumsy', 'cluttered', 'coarse', 'cold', 'colorful', 'colorless', 'colossal', 'comfortable', 'common', 'compassionate', 'competent', 'complete', 'complex', 'complicated', 'composed', 'concerned', 'concrete', 'confused', 'conscious', 'considerate', 'constant']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['body_raw', 'conventional', 'cooked', 'cool', 'cooperative', 'coordinated', 'corny', 'corrupt', 'costly', 'courageous', 'courteous', 'crafty', 'crazy', 'creamy', 'creative', 'creepy', 'criminal', 'crisp', 'critical', 'crooked', 'crowded', 'cruel', 'crushing']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['cuddly', 'cultivated', 'cultured', 'cumbersome', 'curly', 'curvy', 'cute', 'cylindrical', 'damaged', 'damp', 'dangerous', 'dapper', 'daring', 'darling', 'dark', 'dazzling', 'dead', 'deadly', 'deafening', 'dear', 'dearest', 'decent', 'decimal', 'decisive']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['deep', 'defenseless', 'defensive', 'defiant', 'deficient', 'definite', 'definitive', 'delayed', 'delectable', 'delicious', 'delightful', 'delirious', 'demanding', 'dense', 'dental', 'dependable', 'dependent', 'descriptive', 'deserted', 'detailed', 'determined']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['devoted', 'different', 'difficult', 'digital', 'diligent', 'dim', 'dimpled', 'dimwitted', 'direct', 'disastrous', 'discrete', 'disfigured', 'disgusting', 'disloyal', 'dirty', 'disguised', 'dishonest', 'dismal', 'distant', 'distinct', 'distorted', 'dizzy', 'dopey']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['doting', 'double', 'downright', 'drab', 'drafty', 'dramatic', 'dreary', 'droopy', 'dry', 'dual', 'dull', 'dutiful', 'each', 'eager', 'earnest', 'early', 'easy', 'easy-going']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['ecstatic', 'edible', 'educated', 'elaborate', 'elastic', 'elated', 'elderly', 'electric', 'elegant', 'elementary', 'elliptical', 'embarrassed', 'embellished', 'eminent']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['emotional', 'empty', 'enchanted', 'enchanting', 'energetic', 'enlightened', 'enormous', 'enraged', 'entire', 'envious', 'equal', 'equatorial', 'essential', 'esteemed']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['ethical', 'euphoric', 'even', 'evergreen', 'everlasting', 'every', 'evil', 'exalted', 'excellent', 'exemplary', 'exhausted', 'excitable', 'excited', 'exciting', 'exotic']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['expensive', 'experienced', 'expert', 'extraneous', 'extroverted', 'extra-large', 'extra-small', 'fabulous', 'failing', 'faint', 'fair', 'faithful', 'fake', 'false', 'familiar']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['famous', 'fancy', 'fantastic', 'far', 'faraway', 'far-flung', 'far-off', 'fast', 'fat', 'fatal', 'fatherly', 'favorable', 'favorite', 'fearful', 'fearless', 'feisty']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['feline', 'female', 'feminine', 'few', 'fickle', 'filthy', 'fine', 'finished', 'firm', 'first', 'firsthand', 'fitting', 'fixed', 'flaky', 'flamboyant', 'flashy', 'flat']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['flawed', 'flawless', 'flickering', 'flimsy', 'flippant', 'flowery', 'fluffy', 'fluid', 'flustered', 'focused', 'fond', 'foolhardy', 'foolish', 'forceful', 'forked']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['formal', 'forsaken', 'forthright', 'fortunate', 'fragrant', 'frail', 'frank', 'frayed', 'free', 'French', 'fresh', 'frequent', 'friendly', 'frightened', 'frightening']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['frigid', 'frilly', 'frizzy', 'frivolous', 'front', 'frosty', 'frozen', 'frugal', 'fruitful', 'full', 'fumbling', 'functional', 'funny', 'fussy', 'fuzzy', 'gargantuan']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['gaseous', 'general', 'generous', 'gentle', 'genuine', 'giant', 'giddy', 'gigantic', 'gifted', 'giving', 'glamorous', 'glaring', 'glass', 'gleaming', 'gleeful', 'glistening']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['glittering', 'gloomy', 'glorious', 'glossy', 'glum', 'golden', 'good', 'good-natured', 'gorgeous', 'graceful', 'gracious', 'grand', 'grandiose', 'granular', 'grateful']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['grave', 'gray', 'great', 'greedy', 'green', 'gregarious', 'grim', 'grimy', 'gripping', 'grizzled', 'gross', 'grotesque', 'grouchy', 'grounded', 'growing', 'growling']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['grown', 'grubby', 'gruesome', 'grumpy', 'guilty', 'gullible', 'gummy', 'hairy', 'half', 'handmade', 'handsome', 'handy', 'happy', 'happy-go-lucky', 'hard', 'hard-to-find']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['harmful', 'harmless', 'harmonious', 'harsh', 'hasty', 'hateful', 'haunting', 'healthy', 'heartfelt', 'hearty', 'heavenly', 'heavy', 'hefty', 'helpful', 'helpless', 'hidden']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['hideous', 'high', 'high-level', 'hilarious', 'hoarse', 'hollow', 'homely', 'honest', 'honorable', 'honored', 'hopeful', 'horrible', 'hospitable', 'hot', 'huge', 'humble']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['humiliating', 'humming', 'humongous', 'hungry', 'hurtful', 'husky', 'icky', 'icy', 'ideal', 'idealistic', 'identical', 'idle', 'idiotic', 'idolized', 'ignorant', 'ill']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['illegal', 'ill-fated', 'ill-informed', 'illiterate', 'illustrious', 'imaginary', 'imaginative', 'immaculate', 'immaterial', 'immediate', 'immense', 'impassioned']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['impeccable', 'impartial', 'imperfect', 'imperturbable', 'impish', 'impolite', 'important', 'impossible', 'impractical', 'impressionable', 'impressive', 'improbable']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['impure', 'inborn', 'incomparable', 'incompatible', 'incomplete', 'inconsequential', 'incredible', 'indelible', 'inexperienced', 'indolent', 'infamous', 'infantile']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['infatuated', 'inferior', 'infinite', 'informal', 'innocent', 'insecure', 'insidious', 'insignificant', 'insistent', 'instructive', 'insubstantial', 'intelligent', 'intent']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['intentional', 'interesting', 'internal', 'international', 'intrepid', 'ironclad', 'irresponsible', 'irritating', 'itchy', 'jaded', 'jagged', 'jam-packed', 'jaunty']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['jealous', 'jittery', 'joint', 'jolly', 'jovial', 'joyful', 'joyous', 'jubilant', 'judicious', 'juicy', 'jumbo', 'junior', 'jumpy', 'juvenile', 'kaleidoscopic', 'keen']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['key', 'kind', 'kindhearted', 'kindly', 'klutzy', 'knobby', 'knotty', 'knowledgeable', 'knowing', 'known', 'kooky', 'kosher', 'lame', 'lanky', 'large', 'last', 'lasting']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['late', 'lavish', 'lawful', 'lazy', 'leading', 'lean', 'leafy', 'left', 'legal', 'legitimate', 'light', 'lighthearted', 'likable', 'likely', 'limited', 'limp', 'limping']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['linear', 'lined', 'liquid', 'little', 'live', 'lively', 'livid', 'loathsome', 'lone', 'lonely', 'long', 'long-term', 'loose', 'lopsided', 'lost', 'loud', 'lovable']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['lovely', 'loving', 'low', 'loyal', 'lucky', 'lumbering', 'luminous', 'lumpy', 'lustrous', 'luxurious', 'mad', 'made-up', 'magnificent', 'majestic', 'major', 'male']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['mammoth', 'married', 'marvelous', 'masculine', 'massive', 'mature', 'meager', 'mealy', 'mean', 'measly', 'meaty', 'medical', 'mediocre', 'medium', 'meek', 'mellow']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['melodic', 'memorable', 'menacing', 'merry', 'messy', 'metallic', 'mild', 'milky', 'mindless', 'miniature', 'minor', 'minty', 'miserable', 'miserly', 'misguided', 'misty']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['mixed', 'modern', 'modest', 'moist', 'monstrous', 'monthly', 'monumental', 'moral', 'mortified', 'motherly', 'motionless', 'mountainous', 'muddy', 'muffled', 'multicolored']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['mundane', 'murky', 'mushy', 'musty', 'muted', 'mysterious', 'naive', 'narrow', 'nasty', 'natural', 'naughty', 'nautical', 'near', 'neat', 'necessary', 'needy', 'negative']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['neglected', 'negligible', 'neighboring', 'nervous', 'new', 'next', 'nice', 'nifty', 'nimble', 'nippy', 'nocturnal', 'noisy', 'nonstop', 'normal', 'notable', 'noted']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['noteworthy', 'novel', 'noxious', 'numb', 'nutritious', 'nutty', 'obedient', 'obese', 'oily', 'oblong', 'obvious', 'occasional', 'odd', 'oddball', 'offbeat', 'offensive']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['official', 'old', 'old-fashioned', 'only', 'open', 'optimal', 'optimistic', 'opulent', 'orange', 'orderly', 'organic', 'ornate', 'ornery', 'ordinary', 'original', 'other']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['our', 'outlying', 'outgoing', 'outlandish', 'outrageous', 'outstanding', 'oval', 'overcooked', 'overdue', 'overjoyed', 'overlooked', 'palatable', 'pale', 'paltry']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['parallel', 'parched', 'partial', 'passionate', 'past', 'pastel', 'peaceful', 'peppery', 'perfect', 'perfumed', 'periodic', 'perky', 'personal', 'pertinent', 'pesky']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['pessimistic', 'petty', 'phony', 'physical', 'piercing', 'pink', 'pitiful', 'plain', 'plaintive', 'plastic', 'playful', 'pleasant', 'pleased', 'pleasing', 'plump']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['plush', 'polished', 'polite', 'political', 'pointed', 'pointless', 'poised', 'poor', 'popular', 'portly', 'posh', 'positive', 'possible', 'potable', 'powerful']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['powerless', 'practical', 'present', 'prestigious', 'pretty', 'precious', 'previous', 'pricey', 'prickly', 'primary', 'prime', 'pristine', 'private', 'prize']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['probable', 'productive', 'profitable', 'profuse', 'proper', 'proud', 'prudent', 'punctual', 'pungent', 'puny', 'pure', 'purple', 'pushy', 'putrid', 'puzzled']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['puzzling', 'quaint', 'qualified', 'quarrelsome', 'quarterly', 'queasy', 'querulous', 'questionable', 'quick', 'quick-witted', 'quiet', 'quintessential', 'quirky']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['quixotic', 'quizzical', 'radiant', 'ragged', 'rapid', 'rare', 'rash', 'raw', 'recent', 'reckless', 'rectangular', 'ready', 'real', 'realistic', 'reasonable']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['red', 'reflecting', 'regal', 'regular', 'reliable', 'relieved', 'remarkable', 'remorseful', 'remote', 'repentant', 'required', 'respectful', 'responsible']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['repulsive', 'revolving', 'rewarding', 'rich', 'rigid', 'right', 'ringed', 'ripe', 'roasted', 'robust', 'rosy', 'rotating', 'rotten', 'rough', 'round', 'rowdy']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['royal', 'rubbery', 'rundown', 'ruddy', 'rude', 'runny', 'rural', 'rusty', 'sad', 'safe', 'salty', 'same', 'sandy', 'sane', 'sarcastic', 'sardonic', 'satisfied']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['scaly', 'scarce', 'scared', 'scary', 'scented', 'scholarly', 'scientific', 'scornful', 'scratchy', 'scrawny', 'second', 'secondary', 'second-hand', 'secret']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['self-assured', 'self-reliant', 'selfish', 'sentimental', 'separate', 'serene', 'serious', 'serpentine', 'several', 'severe', 'shabby', 'shadowy', 'shady', 'shallow', 'shameful', 'shameless', 'sharp', 'shimmering', 'shiny', 'shocked', 'shocking', 'shoddy', 'short', 'short-term', 'showy', 'shrill', 'shy', 'sick', 'silent', 'silky']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['silly', 'silver', 'similar', 'simple', 'simplistic', 'sinful', 'single', 'sizzling', 'skeletal', 'skinny', 'sleepy', 'slight', 'slim', 'slimy', 'slippery', 'slow']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['slushy', 'small', 'smart', 'smoggy', 'smooth', 'smug', 'snappy', 'snarling', 'sneaky', 'sniveling', 'snoopy', 'sociable', 'soft', 'soggy', 'solid', 'somber']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['some', 'spherical', 'sophisticated', 'sore', 'sorrowful', 'soulful', 'soupy', 'sour', 'Spanish', 'sparkling', 'sparse', 'specific', 'spectacular', 'speedy']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['spicy', 'spiffy', 'spirited', 'spiteful', 'splendid', 'spotless', 'spotted', 'spry', 'square', 'squeaky', 'squiggly', 'stable', 'staid', 'stained', 'stale']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['standard', 'starchy', 'stark', 'starry', 'steep', 'sticky', 'stiff', 'stimulating', 'stingy', 'stormy', 'straight', 'strange', 'steel', 'strict', 'strident']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['striking', 'striped', 'strong', 'studious', 'stunning', 'stupendous', 'stupid', 'sturdy', 'stylish', 'subdued', 'submissive', 'substantial', 'subtle', 'suburban']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['sudden', 'sugary', 'sunny', 'super', 'superb', 'superficial', 'superior', 'supportive', 'sure-footed', 'surprised', 'suspicious', 'svelte', 'sweaty', 'sweet']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['sweltering', 'swift', 'sympathetic', 'tall', 'talkative', 'tame', 'tan', 'tangible', 'tart', 'tasty', 'tattered', 'taut', 'tedious', 'teeming', 'tempting', 'tender']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['tense', 'tepid', 'terrible', 'terrific', 'testy', 'thankful', 'that', 'these', 'thick', 'thin', 'third', 'thirsty', 'this', 'thorough', 'thorny', 'those', 'thoughtful', 'threadbare', 'thrifty', 'thunderous', 'tidy', 'tight', 'timely', 'tinted', 'tiny', 'tired', 'torn', 'total', 'tough', 'traumatic', 'treasured', 'tragic', 'trained']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'tremendous', 'triangular', 'tricky', 'trifling', 'trim', 'trivial', 'troubled', 'true', 'trusting', 'trustworthy', 'trusty', 'truthful', 'tubby', 'turbulent', 'twin']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['ugly', 'ultimate', 'unacceptable', 'unaware', 'uncomfortable', 'uncommon', 'unconscious', 'understated', 'unequaled', 'uneven', 'unfinished', 'unfit']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['unfolded', 'unfortunate', 'unhappy', 'unhealthy', 'uniform', 'unimportant', 'unique', 'united', 'unkempt', 'unknown', 'unlawful', 'unlined', 'unlucky']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['unnatural', 'unpleasant', 'unrealistic', 'unripe', 'unruly', 'unselfish', 'unsightly', 'unsteady', 'unsung', 'untidy', 'untimely', 'untried', 'untrue']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY['unused', 'unusual', 'unwelcome', 'unwieldy', 'unwilling', 'unwitting', 'unwritten', 'upbeat', 'upright', 'upset', 'urban', 'usable', 'used', 'useful']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'useless', 'utilized', 'utter', 'vacant', 'vague', 'vain', 'valid', 'valuable', 'vapid', 'variable', 'vast', 'velvety', 'venerated', 'vengeful', 'verifiable']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'vibrant', 'vicious', 'victorious', 'vigilant', 'vigorous', 'villainous', 'violet', 'violent', 'virtual', 'virtuous', 'visible', 'vital', 'vivacious', 'vivid']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'voluminous', 'wan', 'warlike', 'warm', 'warmhearted', 'warped', 'wary', 'wasteful', 'watchful', 'waterlogged', 'watery', 'wavy', 'wealthy', 'weak', 'weary']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'webbed', 'wee', 'weekly', 'weepy', 'weighty', 'weird', 'welcome', 'well-documented', 'well-groomed', 'well-informed', 'well-lit', 'well-made', 'well-off']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'well-to-do', 'well-worn', 'wet', 'which', 'whimsical', 'whirlwind', 'whispered', 'white', 'whole', 'whopping', 'wicked', 'wide', 'wide-eyed', 'wiggly', 'wild']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'willing', 'wilted', 'winding', 'windy', 'winged', 'wiry', 'wise', 'witty', 'wobbly', 'woeful', 'wonderful', 'wooden', 'woozy', 'wordy', 'worldly', 'worn']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'worried', 'worrisome', 'worse', 'worst', 'worthless', 'worthwhile', 'worthy', 'wrathful', 'wretched', 'writhing', 'wrong', 'wry', 'yawning', 'yearly', 'yellow']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ADJECTIVE', UNNEST(ARRAY[ 'yellowish', 'young', 'youthful', 'yummy', 'zany', 'zealous', 'zesty', 'zigzag']));

-- inserting 221 animals
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['aardvark', 'albatross', 'alligator', 'alpaca', 'ant', 'anteater', 'antelope', 'ape', 'armadillo', 'donkey', 'baboon', 'badger', 'barracuda', 'bat', 'bear', 'beaver']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['bee', 'bison', 'boar', 'buffalo', 'butterfly', 'camel', 'capybara', 'caribou', 'cassowary', 'cat', 'caterpillar', 'cattle', 'chamois', 'cheetah', 'chicken']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['chimpanzee', 'chinchilla', 'chough', 'clam', 'cobra', 'cockroach', 'cod', 'cormorant', 'coyote', 'crab', 'crane', 'crocodile', 'crow', 'curlew', 'deer', 'dinosaur']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['dog', 'dogfish', 'dolphin', 'dotterel', 'dove', 'dragonfly', 'duck', 'dugong', 'dunlin', 'eagle', 'echidna', 'eel', 'eland', 'elephant', 'elk', 'emu', 'falcon']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['ferret', 'finch', 'fish', 'flamingo', 'fly', 'fox', 'frog', 'gaur', 'gazelle', 'gerbil', 'giraffe', 'gnat', 'gnu', 'goat', 'goldfinch', 'goldfish', 'goose']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['gorilla', 'goshawk', 'grasshopper', 'grouse', 'guanaco', 'gull', 'hamster', 'hare', 'hawk', 'hedgehog', 'heron', 'herring', 'hippopotamus', 'hornet', 'horse']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['human', 'hummingbird', 'hyena', 'ibex', 'ibis', 'jackal', 'jaguar', 'jay', 'jellyfish', 'kangaroo', 'kingfisher', 'koala', 'kookabura', 'kouprey', 'kudu', 'lapwing']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['lark', 'lemur', 'leopard', 'lion', 'llama', 'lobster', 'locust', 'loris', 'louse', 'lyrebird', 'magpie', 'mallard', 'manatee', 'mandrill', 'mantis', 'marten']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['meerkat', 'mink', 'mole', 'mongoose', 'monkey', 'moose', 'mosquito', 'mouse', 'mule', 'narwhal', 'newt', 'nightingale', 'octopus', 'okapi', 'opossum', 'oryx']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['ostrich', 'otter', 'owl', 'oyster', 'panther', 'parrot', 'partridge', 'peafowl', 'pelican', 'penguin', 'pheasant', 'pig', 'pigeon', 'pony', 'porcupine', 'porpoise']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['quail', 'quelea', 'quetzal', 'rabbit', 'raccoon', 'rail', 'ram', 'rat', 'raven', 'reindeer', 'rhinoceros', 'rook', 'salamander', 'salmon', 'sandpiper', 'sardine']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['scorpion', 'seahorse', 'seal', 'shark', 'sheep', 'shrew', 'skunk', 'snail', 'snake', 'sparrow', 'spider', 'spoonbill', 'squid', 'squirrel', 'starling', 'stingray']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['stinkbug', 'stork', 'swallow', 'swan', 'tapir', 'tarsier', 'termite', 'tiger', 'toad', 'trout', 'turkey', 'turtle', 'viper', 'vulture', 'wallaby', 'walrus', 'wasp']));
INSERT INTO posts_utils.terms(term_type, term) VALUES('ANIMAL', UNNEST(ARRAY['weasel', 'whale', 'wildcat', 'wolf', 'wolverine', 'wombat', 'woodcock', 'woodpecker', 'worm', 'wren', 'yak', 'zebra']));


DROP FUNCTION IF EXISTS posts_utils.get_random_nickname;
CREATE OR REPLACE FUNCTION posts_utils.get_random_nickname() RETURNS TEXT 
AS $$ 
DECLARE 
    adjective text;
    animal text;
BEGIN
  SELECT
    term FROM posts_utils.terms
    WHERE term_type = 'ADJECTIVE'
    OFFSET floor(random() * 1338)
    LIMIT 1 
    INTO adjective;
  SELECT
    term FROM posts_utils.terms
    WHERE term_type = 'ANIMAL'
    OFFSET floor(random() * 220)
    LIMIT 1
    INTO animal;
  RETURN CONCAT (adjective, '-', animal);
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION posts_utils.get_random_nickname IS 'Returns a random adjective + animal to use as nickname';

-- Automatically update modified_at/updated_at field

DROP FUNCTION IF EXISTS public.sync_last_modification;
CREATE OR REPLACE FUNCTION public.sync_last_modification()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = now();
RETURN NEW;
END;
$$ language plpgsql; 
COMMENT ON FUNCTION public.sync_last_modification IS 'A trigger to use on any table that uses a ''created_at'' field';
-- syntax:
-- CREATE TRIGGER on_update_sync_time BEFORE UPDATE ON <table> FOR EACH ROW EXECUTE PROCEDURE sync_last_modification();
