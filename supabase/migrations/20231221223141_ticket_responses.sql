CREATE TABLE public.tickets_responses(
  response_id       BIGINT                 PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  ticket_id         BIGINT                 NOT NULL REFERENCES public.tickets(ticket_id) ON DELETE CASCADE,
  user_id           UUID                   REFERENCES auth.users(id) ON DELETE SET NULL,
  moderator_id      UUID                   REFERENCES auth.users(id) ON DELETE SET NULL,
	created_at        TIMESTAMP(3)           NOT NULL DEFAULT current_timestamp,
  body_raw          TEXT                   NOT NULL DEFAULT '',
  notes              TEXT
);

COMMENT ON TABLE  public.tickets_responses                   IS 'Stores users tickets';
COMMENT ON COLUMN public.tickets_responses.response_id       IS 'The ticket''s unique id';
COMMENT ON COLUMN public.tickets_responses.ticket_id         IS 'The ticket''s unique id';
COMMENT ON COLUMN public.tickets_responses.user_id           IS 'The ticket''s user id. Not necessary';
COMMENT ON COLUMN public.tickets_responses.moderator_id      IS 'The ticket''s user id. Not necessary';
COMMENT ON COLUMN public.tickets_responses.created_at        IS 'The ticket''s creation date';
COMMENT ON COLUMN public.tickets_responses.body_raw          IS 'The ticket''s body, as inserted by the user';
COMMENT ON COLUMN public.tickets_responses.notes             IS 'Any note moderators want to write';

-------------------------------------------------------------------------------
-- SECURITY/RLS ---------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.tickets_responses ENABLE ROW LEVEL SECURITY;

DROP POLICY IF EXISTS moderators_can_use_tickets_responses ON public.tickets_responses;
CREATE POLICY moderators_can_use_tickets_responses
ON public.tickets_responses FOR ALL TO authenticated
USING (is_moderator() )
WITH CHECK (is_moderator() );