-- UPVOTES

-------------------------------------------------------------------------------
-- TABLES ---------------------------------------------------------------------
-------------------------------------------------------------------------------


DROP TABLE IF EXISTS public.votes;
CREATE TABLE public.votes (
  post_id           BIGINT        REFERENCES public.posts(post_id) ON DELETE SET NULL,
  user_id           UUID          REFERENCES auth.users(id) ON DELETE SET NULL,
  created_at        TIMESTAMP(3)  NOT NULL DEFAULT current_timestamp,
  updated_at        TIMESTAMP(3)  NOT NULL DEFAULT current_timestamp,
  vote_value INT    NOT           NULL DEFAULT 1,
  PRIMARY KEY (post_id, user_id),
  CONSTRAINT vote_value_amount CHECK (vote_value = 1 OR vote_value = -1 OR vote_value = 0)
);
COMMENT ON TABLE  public.votes            IS 'Counts upvotes and downvotes between users and posts.';
COMMENT ON COLUMN public.votes.post_id    IS 'The post';
COMMENT ON COLUMN public.votes.user_id    IS 'The user';
COMMENT ON COLUMN public.votes.created_at IS 'The time the user voted';
COMMENT ON COLUMN public.votes.updated_at IS 'The time the user changed their vote';
COMMENT ON COLUMN public.votes.vote_value IS 'Determines if this is an upvote or downvote';

-------------------------------------------------------------------------------
-- SPECIAL RULES/INDICES ------------------------------------------------------
-------------------------------------------------------------------------------

CREATE TRIGGER on_vote_update_sync_time BEFORE UPDATE ON public.posts FOR EACH ROW EXECUTE PROCEDURE sync_last_modification();

-------------------------------------------------------------------------------
-- RLS ------------------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.votes ENABLE ROW LEVEL SECURITY;

DROP POLICY IF EXISTS anon_can_see_votes ON public.votes;
CREATE POLICY anon_can_see_votes
ON public.votes FOR SELECT
USING ( true );

DROP POLICY IF EXISTS owner_can_insert_new_vote ON public.votes;
CREATE POLICY owner_can_insert_new_vote
ON public.votes FOR INSERT
WITH CHECK ( auth.uid() = user_id );

-- This is ok thanks to the CONSTRAINT which means users cannot increase their vote score over a unit.
DROP POLICY IF EXISTS owner_can_update_own_vote ON public.votes;
CREATE POLICY owner_can_update_own_vote
ON public.votes FOR UPDATE
USING ( auth.uid() = user_id );

DROP POLICY IF EXISTS owner_can_delete_own_vote ON public.votes;
CREATE POLICY owner_can_delete_own_vote
ON public.votes FOR DELETE
USING ( auth.uid() = user_id );

-------------------------------------------------------------------------------
-- FUNCTIONS ------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.vote;
CREATE OR REPLACE FUNCTION public.vote(
  post_id    BIGINT,
  vote_value INT   DEFAULT 1,
	user_id    UUID  DEFAULT auth.uid()
) RETURNS void
AS $$
#variable_conflict use_column
BEGIN
  INSERT INTO public.votes
    (post_id, user_id, vote_value)
  VALUES
    (vote.post_id, vote.user_id, vote.vote_value)
  ON CONFLICT (post_id, user_id) DO UPDATE
    SET vote_value = EXCLUDED.vote_value;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.vote IS 'Upvotes or downvotes a post';