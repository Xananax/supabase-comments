DROP TYPE IF EXISTS public.TICKET_STATUS;
CREATE TYPE public.TICKET_STATUS AS ENUM ('WAITING', 'PROCESSING', 'HALTED', 'ARCHIVED'); -- noqa

DROP TABLE IF EXISTS public.tickets;
CREATE TABLE public.tickets (
  ticket_id         BIGINT        PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id           UUID          REFERENCES auth.users(id) ON DELETE SET NULL,
	user_email        TEXT          NOT NULL,
  sent_at           TIMESTAMP(3)  NOT NULL,
  created_at        TIMESTAMP(3)  NOT NULL DEFAULT current_timestamp,
  body_raw          TEXT          NOT NULL DEFAULT '',
  reason            TEXT          NULL,
  name              TEXT          NULL,
  status            TICKET_STATUS NOT NULL DEFAULT 'WAITING'::TICKET_STATUS,
  title             TEXT,
  flagged_reason    TEXT,
  notes             TEXT
);

COMMENT ON TABLE  public.tickets                   IS 'Stores users tickets';
COMMENT ON COLUMN public.tickets.ticket_id         IS 'The ticket''s unique id';
COMMENT ON COLUMN public.tickets.user_id           IS 'The ticket''s user id. Not necessary';
COMMENT ON COLUMN public.tickets.user_email        IS 'The ticket''s user email, as provided in the form. If the user is logged out, this would be a different email';
COMMENT ON COLUMN public.tickets.sent_at           IS 'Supposed to be filled by the UI when the page loads. Used to detect fast entry';
COMMENT ON COLUMN public.tickets.name              IS 'Honeypot for tickets. This HAS to be NULL';
COMMENT ON COLUMN public.tickets.created_at        IS 'The ticket''s creation date';
COMMENT ON COLUMN public.tickets.body_raw          IS 'The ticket''s body, as inserted by the user';
COMMENT ON COLUMN public.tickets.status            IS 'Tracks where the ticket''s at';
COMMENT ON COLUMN public.tickets.title             IS 'Optional title/reason';
COMMENT ON COLUMN public.tickets.flagged_reason    IS 'If the ticket is flagged, the reason will be here';
COMMENT ON COLUMN public.tickets.notes             IS 'Any note moderators want to write';

-------------------------------------------------------------------------------
-- SECURITY/RLS ---------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.tickets ENABLE ROW LEVEL SECURITY;

-- We want to make sure anonymous users can't write to some of the columns
DROP POLICY IF EXISTS anon_can_create_new_ticket ON public.posts;
CREATE POLICY anon_can_create_new_ticket
ON public.tickets FOR INSERT
WITH CHECK (
  flagged_reason IS NULL
  AND
  notes IS NULL
  AND 
  status = 'WAITING'::TICKET_STATUS
);
COMMENT ON POLICY anon_can_create_new_ticket ON public.tickets IS 'Make sure the moderator columns aren''t overwritten';


-- we want to try to guess if a ticket might be spam
DROP FUNCTION IF EXISTS check_if_ticket_might_be_spam;
CREATE OR REPLACE FUNCTION check_if_ticket_might_be_spam()
RETURNS TRIGGER AS $$
BEGIN
  IF NEW.name <> '' THEN
    NEW.flagged_reason := 'honeypot';
  END IF;

  IF NEW.sent_at IS NULL THEN
    IF NEW.flagged_reason IS NULL THEN
      NEW.flagged_reason := 'sent time missing';
    ELSE
      NEW.flagged_reason := NEW.flagged_reason || ', sent time missing';
    END IF;
  ELSIF ABS(EXTRACT(EPOCH FROM NEW.sent_at - NEW.created_at)) < 1 THEN
    IF NEW.flagged_reason IS NULL THEN
      NEW.flagged_reason := 'too fast';
    ELSE
      NEW.flagged_reason := NEW.flagged_reason || ', too fast';
    END IF;
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION check_if_ticket_might_be_spam IS 'Verifies if the ticket is not spam';


DROP TRIGGER IF EXISTS check_if_ticket_might_be_spam_on_insert ON public.tickets;
CREATE TRIGGER check_if_ticket_might_be_spam_on_insert
BEFORE INSERT ON public.tickets
FOR EACH ROW
EXECUTE FUNCTION check_if_ticket_might_be_spam();

-- we want ticket openers to be able to see their ticket, as well as moderators

DROP POLICY IF EXISTS owner_or_mod_can_see_tickets ON public.tickets;
CREATE POLICY owner_or_mod_can_see_tickets
ON public.tickets FOR SELECT
USING ( 
  ( auth.uid() = user_id )
  OR
  ( is_moderator() )
);

-- we want mods to be able to update any ticket

DROP POLICY IF EXISTS mod_can_update_ticket ON public.tickets;
CREATE POLICY mod_can_update_ticket
ON public.tickets FOR UPDATE
USING ( is_moderator() );


-------------------------------------------------------------------------------
-- UTILITY FUNCTIONS ----------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.create_ticket;
CREATE OR REPLACE FUNCTION public.create_ticket(
  user_email   TEXT,
  body_raw     TEXT,
  title        TEXT DEFAULT '',
  sent_at      TIMESTAMP(3) DEFAULT current_timestamp,
  name         TEXT DEFAULT NULL,
  user_id      UUID DEFAULT NULL
) RETURNS void
AS $$
BEGIN
  INSERT INTO public.tickets (
    user_id,
    user_email,
    body_raw,
    sent_at,
    name,
    title
  )
  VALUES (
    user_id,
    user_email,
    body_raw,
    sent_at,
    name,
    title
  );
END;
$$ LANGUAGE plpgsql;
COMMENT ON FUNCTION public.create_ticket IS 'Creates a ticket';