-- TAGS
-- https://www.crunchydata.com/blog/tags-aand-postgres-arrays-a-purrfect-combination


DROP TABLE IF EXISTS public.tags;
CREATE TABLE public.tags (
    tag_id   BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    tag_slug TEXT   NOT NULL,
    unique(tag_slug)
);
ALTER TABLE public.tags ENABLE ROW LEVEL SECURITY;

COMMENT ON TABLE  public.tags          IS 'Stores tags';
COMMENT ON COLUMN public.tags.tag_id   IS 'The tags''s unique id';
COMMENT ON COLUMN public.tags.tag_slug IS 'The tags''s unique slug. Used to display to the user.';

DROP POLICY IF EXISTS anon_can_see_tags ON public.tags;
CREATE POLICY anon_can_see_profiles
ON public.tags FOR SELECT
USING ( true );