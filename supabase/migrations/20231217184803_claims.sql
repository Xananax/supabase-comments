-------------------------------------------------------------------------------
-- SECURITY/ROLES -------------------------------------------------------------
-------------------------------------------------------------------------------

-- https://github.com/supabase-community/supabase-custom-claims

DROP FUNCTION IF EXISTS is_claims_admin;
CREATE OR REPLACE FUNCTION is_claims_admin() RETURNS bool
  LANGUAGE "plpgsql" 
  AS $$
  BEGIN
    IF session_user = 'authenticator' THEN
      --------------------------------------------
      -- To disallow any authenticated app users
      -- from editing claims, delete the following
      -- block of code and replace it with:
      -- RETURN FALSE;
      --------------------------------------------
      IF extract(epoch from now()) > coalesce((current_setting('request.jwt.claims', true)::jsonb)->>'exp', '0')::numeric THEN
        return false; -- jwt expired
      END IF;
      If current_setting('request.jwt.claims', true)::jsonb->>'role' = 'service_role' THEN
        RETURN true; -- service role users have admin rights
      END IF;
      IF coalesce((current_setting('request.jwt.claims', true)::jsonb)->'app_metadata'->'claims_admin', 'false')::bool THEN
        return true; -- user has claims_admin set to true
      ELSE
        return false; -- user does NOT have claims_admin set to true
      END IF;
      --------------------------------------------
      -- End of block 
      --------------------------------------------
    ELSE -- not a user session, probably being called from a trigger or something
      return true;
    END IF;
  END;
$$;
COMMENT ON FUNCTION is_claims_admin IS 'Returns true if the user can set claims to themselves';

DROP FUNCTION IF EXISTS get_my_claims;
CREATE OR REPLACE FUNCTION get_my_claims() RETURNS jsonb
    LANGUAGE "sql" STABLE
    AS $$
  SELECT 
  	coalesce(nullif(current_setting('request.jwt.claims', true), '')::jsonb -> 'app_metadata', '{}'::jsonb)::jsonb
$$;
COMMENT ON FUNCTION get_my_claims IS 'Returns all the currently authenticated user''s claims';

DROP FUNCTION IF EXISTS get_my_claim;
CREATE OR REPLACE FUNCTION get_my_claim(claim TEXT) RETURNS jsonb
    LANGUAGE "sql" STABLE
    AS $$
  SELECT 
    coalesce(nullif(current_setting('request.jwt.claims', true), '')::jsonb -> 'app_metadata' -> claim, null)
$$;
COMMENT ON FUNCTION get_my_claims IS 'Returns an authenticated user''s specific claim';

DROP FUNCTION IF EXISTS get_claims;
CREATE OR REPLACE FUNCTION get_claims(uid uuid) RETURNS jsonb
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    DECLARE retval jsonb;
    BEGIN
      IF NOT is_claims_admin() THEN
          RETURN '{"error":"access denied"}'::jsonb;
      ELSE
        SELECT raw_app_meta_data from auth.users into retval where id = uid::uuid;
        return retval;
      END IF;
    END;
$$;
COMMENT ON FUNCTION get_claims IS 'Returns the claims for the specified user';

DROP FUNCTION IF EXISTS get_claim;
CREATE OR REPLACE FUNCTION get_claim(uid UUID, claim text) RETURNS jsonb
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    DECLARE retval jsonb;
    BEGIN
      IF NOT is_claims_admin() THEN
          RETURN '{"error":"access denied"}'::jsonb;
      ELSE
        SELECT coalesce(raw_app_meta_data->claim, null) from auth.users into retval where id = uid::uuid;
        return retval;
      END IF;
    END;
$$;
COMMENT ON FUNCTION get_claim IS 'Returns the specific claim for the specified user';

DROP FUNCTION IF EXISTS set_claim;
CREATE OR REPLACE FUNCTION set_claim(uid UUID, claim text, value jsonb) RETURNS TEXT
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    BEGIN
      IF NOT is_claims_admin() THEN
          RETURN 'error: access denied';
      ELSE        
        update auth.users set raw_app_meta_data = 
          raw_app_meta_data || 
            json_build_object(claim, value)::jsonb where id = uid;
        return 'OK';
      END IF;
    END;
$$;
COMMENT ON FUNCTION set_claim IS 'Provides a claim to a specified user';

DROP FUNCTION IF EXISTS delete_claim;
CREATE OR REPLACE FUNCTION delete_claim(uid UUID, claim text) RETURNS TEXT
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    BEGIN
      IF NOT is_claims_admin() THEN
          RETURN 'error: access denied';
      ELSE        
        update auth.users set raw_app_meta_data = 
          raw_app_meta_data - claim where id = uid;
        return 'OK';
      END IF;
    END;
$$;
COMMENT ON FUNCTION delete_claim IS 'Removes a claim from a specified user';

DROP FUNCTION IF EXISTS elevate_to_claim_admin;
CREATE OR REPLACE FUNCTION elevate_to_claim_admin(uid UUID) RETURNS TEXT
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    BEGIN
      RETURN set_claim(uid, 'claims_admin', 'true');
    END;
$$;
COMMENT ON FUNCTION elevate_to_claim_admin IS 'Sets a user AS being able to set and remove claims';

DROP FUNCTION IF EXISTS set_role;
CREATE OR REPLACE FUNCTION set_role(uid UUID, role TEXT) RETURNS TEXT
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    BEGIN
      RETURN set_claim(uid, 'userrole', role);
    END;
$$;
COMMENT ON FUNCTION set_role IS 'Sets a role on a user';

DROP FUNCTION IF EXISTS set_moderator;
CREATE OR REPLACE FUNCTION set_moderator(uid UUID) RETURNS TEXT
    LANGUAGE "plpgsql" SECURITY DEFINER SET search_path = public
    AS $$
    BEGIN
      RETURN set_claim(uid, 'userrole', '"MODERATOR"');
    END;
$$;
COMMENT ON FUNCTION set_moderator IS 'Makes a user a moderator';

DROP FUNCTION IF EXISTS is_moderator;
CREATE OR REPLACE FUNCTION is_moderator()
RETURNS BOOL
AS $$
    BEGIN
    -- INSERT INTO logs (text) VALUES(((get_my_claim('userrole'::text)) = '"MODERATOR"'::jsonb ));
      RETURN ((get_my_claim('userrole'::text)) = '"MODERATOR"'::jsonb );
    END;
$$
LANGUAGE "plpgsql";
COMMENT ON FUNCTION set_moderator IS 'Returns true if the currently logged user is a moderator';

NOTIFY pgrst, 'reload schema';