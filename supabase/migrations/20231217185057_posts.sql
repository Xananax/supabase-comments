-- POSTS

-------------------------------------------------------------------------------
-- TABLES ---------------------------------------------------------------------
-------------------------------------------------------------------------------

-- if not using Supabase, the uuid extension needs to be enabled with:
-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

DROP TABLE IF EXISTS public.posts;
CREATE TABLE public.posts (
  post_id           BIGINT       PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id           UUID         REFERENCES auth.users(id) ON DELETE SET NULL,
  created_at        TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  updated_at        TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  previous_versions TEXT ARRAY   NOT NULL DEFAULT '{}',
  body_raw          TEXT         NOT NULL DEFAULT '',
  is_published      BOOLEAN      NOT NULL DEFAULT true,
  title             TEXT         NOT NULL,
  post_slug         TEXT         NOT NULL,
  post_tags         BIGINT[],
  reply_to_id       BIGINT       NULL REFERENCES public.posts(post_id),
  deleted_at        TIMESTAMP(3) NULL
);
COMMENT ON TABLE  public.posts                   IS 'Stores posts';
COMMENT ON COLUMN public.posts.post_id           IS 'The post''s unique id';
COMMENT ON COLUMN public.posts.user_id           IS 'The post''s user id.';
COMMENT ON COLUMN public.posts.created_at        IS 'The post''s creation date';
COMMENT ON COLUMN public.posts.updated_at        IS 'The post''s last modification date';
COMMENT ON COLUMN public.posts.previous_versions IS 'The post''s text prior to modification';
COMMENT ON COLUMN public.posts.body_raw          IS 'The post''s body, as inserted by the user';
COMMENT ON COLUMN public.posts.is_published      IS 'The post''s published or draft status';
COMMENT ON COLUMN public.posts.deleted_at        IS 'If not null, the post was deleted';
COMMENT ON COLUMN public.posts.reply_to_id       IS 'The post''s the parent id, if the comment is not top-level';
COMMENT ON COLUMN public.posts.title             IS 'The post''s title';
COMMENT ON COLUMN public.posts.post_tags         IS 'The post''s array of tags';
COMMENT ON COLUMN public.posts.post_slug         IS 'The slug/url the post is attached to';

-------------------------------------------------------------------------------
-- SPECIAL RULES/INDICES ------------------------------------------------------
-------------------------------------------------------------------------------

CREATE INDEX idx_posts_slug_index        ON public.posts (post_slug);
CREATE INDEX idx_posts_tags_array_index  ON public.posts USING GIN (post_tags);
CREATE INDEX idx_posts_reply_to_id_index ON public.posts (reply_to_id);
CREATE INDEX idx_posts_user_id_index     ON public.posts (user_id);
CREATE INDEX idx_posts_created_at_index  ON public.posts (created_at desc);
CREATE TRIGGER on_posts_update_sync_time BEFORE UPDATE ON public.posts FOR EACH ROW EXECUTE PROCEDURE sync_last_modification();

-------------------------------------------------------------------------------
-- LOCK COLUMNS ---------------------------------------------------------------
-------------------------------------------------------------------------------

-- see: https://stackoverflow.com/questions/72756376/supabase-solutions-for-column-level-security
-- see: https://github.com/orgs/supabase/discussions/656#discussioncomment-5594653
-- see: https://github.com/orgs/supabase/discussions/656#discussioncomment-4974429
DROP FUNCTION IF EXISTS is_post_update_valid;
CREATE OR REPLACE FUNCTION is_post_update_valid(
    post_id     BIGINT,
    reply_to_id BIGINT
) RETURNS BOOLEAN AS
$$
  WITH original_row AS (
    SELECT reply_to_id
    FROM public.posts
    WHERE public.posts.post_id = is_post_update_valid.post_id
  )
  SELECT(
    (SELECT reply_to_id FROM original_row) = is_post_update_valid.reply_to_id
  );
$$ LANGUAGE SQL SECURITY DEFINER;
COMMENT ON FUNCTION is_post_update_valid IS 'Checks if the user is not updating fields that should be moderator only';

-------------------------------------------------------------------------------
-- SECURITY/RLS ---------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.posts ENABLE ROW LEVEL SECURITY;

DROP POLICY IF EXISTS anon_can_see_posts ON public.posts;
CREATE POLICY anon_can_see_posts
ON public.posts FOR SELECT
USING ( true );

DROP POLICY IF EXISTS authenticated_can_create_new_post ON public.posts;
CREATE POLICY authenticated_can_create_new_post
ON public.posts FOR INSERT TO authenticated
WITH CHECK ( auth.uid() = user_id );

DROP POLICY IF EXISTS owner_or_mod_can_update_own_post ON public.posts;
CREATE POLICY owner_or_mod_can_update_own_post
ON public.posts FOR UPDATE
USING ( 
  (
    auth.uid() = user_id 
    AND
    is_post_update_valid(post_id, reply_to_id)
  )
  OR 
  ( is_moderator() )
);

-------------------------------------------------------------------------------
-- TRIGGER/VERSIONING ---------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS public.keep_previous_post_version;
CREATE OR REPLACE FUNCTION public.keep_previous_post_version()
RETURNS TRIGGER AS $$
BEGIN
  IF OLD.body_raw <> NEW.body_raw THEN
    NEW.previous_versions = array_append(OLD.previous_versions, OLD.body_raw);
  END IF;
RETURN NEW;
END;
$$ language plpgsql; 
COMMENT ON FUNCTION public.keep_previous_post_version IS 'A trigger to use when updating a post';

CREATE TRIGGER on_post_update_keep_previous_version BEFORE UPDATE ON public.posts FOR EACH ROW EXECUTE PROCEDURE public.keep_previous_post_version();
