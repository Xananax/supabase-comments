-------------------------------------------------------------------------------
-- TABLES ---------------------------------------------------------------------
-------------------------------------------------------------------------------

-- PROFILES

DROP TABLE IF EXISTS public.profiles;
CREATE TABLE public.profiles (
  user_id         UUID         NOT NULL REFERENCES auth.users(id) ON DELETE CASCADE,
  created_at      TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  updated_at      TIMESTAMP(3) NOT NULL DEFAULT current_timestamp,
  hide_info       BOOLEAN      NOT NULL DEFAULT false,
  nickname        TEXT,
  stripe_customer TEXT,
	avatar_url      TEXT,
  PRIMARY KEY (user_id)
);

COMMENT ON TABLE public.profiles                  IS 'Stores additional information about users';
COMMENT ON COLUMN public.profiles.user_id         IS 'Links a profile to a user';
COMMENT ON COLUMN public.profiles.created_at      IS 'Profile creation date';
COMMENT ON COLUMN public.profiles.created_at      IS 'Profile update date';
COMMENT ON COLUMN public.profiles.nickname        IS 'User''s nickname. Not necessarily unique, not a discriminant';
COMMENT ON COLUMN public.profiles.stripe_customer IS 'Example of additional field with profile-only data. Remove if not necessary';
COMMENT ON COLUMN public.profiles.hide_info       IS 'If true, the user''s nickname is hidden';
COMMENT ON COLUMN public.profiles.avatar_url      IS 'User''s avatar';

-------------------------------------------------------------------------------
-- SPECIAL RULES --------------------------------------------------------------
-------------------------------------------------------------------------------

ALTER TABLE public.profiles ENABLE ROW LEVEL SECURITY;
CREATE TRIGGER on_profiles_update_sync_time BEFORE UPDATE ON public.profiles FOR EACH ROW EXECUTE PROCEDURE sync_last_modification();

-------------------------------------------------------------------------------
-- ROW LEVEL SECURITY ---------------------------------------------------------
-------------------------------------------------------------------------------

DROP POLICY IF EXISTS anon_can_see_profiles ON public.profiles;
CREATE POLICY anon_can_see_profiles
ON public.profiles FOR SELECT
USING ( true );

DROP POLICY IF EXISTS owner_can_insert_new_profile ON public.profiles;
CREATE POLICY owner_can_insert_new_profile
ON public.profiles FOR INSERT
WITH CHECK ( auth.uid() = user_id );

DROP POLICY IF EXISTS owner_can_update_own_profile ON public.profiles;
CREATE POLICY owner_can_update_own_profile
ON public.profiles FOR UPDATE
USING ( auth.uid() = user_id );

-------------------------------------------------------------------------------
-- TRIGGER --------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS auth.create_profile_for_user;
CREATE OR REPLACE FUNCTION auth.create_profile_for_user()
RETURNS TRIGGER
AS $$
BEGIN
  INSERT INTO public.profiles (user_id)
  VALUES (NEW.id);
  RETURN NEW;
END;
$$
LANGUAGE plpgsql
SECURITY DEFINER
SET search_path = public;

COMMENT ON FUNCTION auth.create_profile_for_user IS 'Adds a profile for a given user ID';

DROP TRIGGER IF EXISTS on_auth_user_created ON auth.users;
CREATE OR REPLACE TRIGGER on_auth_user_created
AFTER INSERT ON auth.users
FOR EACH ROW
EXECUTE FUNCTION auth.create_profile_for_user();

COMMENT ON TRIGGER on_auth_user_created ON auth.users IS 'Adds a profile when a user is added';


-------------------------------------------------------------------------------
-- TRIGGER --------------------------------------------------------------------
-------------------------------------------------------------------------------

DROP FUNCTION IF EXISTS auth.create_user;
CREATE OR REPLACE FUNCTION auth.create_user(
  email    TEXT,
  password TEXT,
  nickname TEXT DEFAULT ''
) RETURNS UUID
SECURITY DEFINER
AS $$
DECLARE
  user_id UUID;
  encrypted_pw text;
  user_meta_data jsonb;
BEGIN
  user_id := gen_random_UUID();
  encrypted_pw := crypt(password, gen_salt('bf'));
  
  INSERT INTO auth.users
    (instance_id, id, aud, role, email, encrypted_password, email_confirmed_at, recovery_sent_at, last_sign_in_at, raw_app_meta_data, raw_user_meta_data, created_at, updated_at, confirmation_token, email_change, email_change_token_new, recovery_token)
  VALUES
    ('00000000-0000-0000-0000-000000000000', user_id, 'authenticated', 'authenticated', email, encrypted_pw, now(), now(), now(), '{"provider":"email","providers":["email"]}', '{}', now(), now(), '', '', '', '');
  
  INSERT INTO auth.identities (id, user_id, identity_data, provider, last_sign_in_at, created_at, updated_at)
  VALUES
    (gen_random_UUID(), user_id, format('{"sub":"%s","email":"%s"}', user_id::text, email)::jsonb, 'email', now(), now(), now());

  RETURN user_id;
END;
$$ LANGUAGE plpgsql;

COMMENT ON FUNCTION auth.create_user IS 'Adds a user. This is NOT supposed to be used in production, only for testing';