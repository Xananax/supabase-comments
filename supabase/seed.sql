-------------------------------------------------------------------------------
-- USERS ----------------------------------------------------------------------
-------------------------------------------------------------------------------

SELECT auth.create_user(email => 'test_a@test.com', password => 'aaaaaa');
SELECT auth.create_user(email => 'test_b@test.com', password => 'bbbbbb');
SELECT auth.create_user(email => 'test_c@test.com', password => 'cccccc');


-------------------------------------------------------------------------------
-- TAGS -----------------------------------------------------------------------
-------------------------------------------------------------------------------

INSERT INTO public.tags(tag_slug) (SELECT UNNEST(ARRAY['fun', 'sad', 'tech', 'maths', 'gossip', 'clarification', 'question']));

-------------------------------------------------------------------------------
-- MAKE ADMIN -----------------------------------------------------------------
-------------------------------------------------------------------------------

SELECT set_moderator(
  auth.create_user(email => 'admin@test.com', password => '111111')
);

-------------------------------------------------------------------------------
-- COMMENTS -------------------------------------------------------------------
-------------------------------------------------------------------------------


SELECT create_thread(
  title => 'testing this',
  post_slug => '/test',
  body_raw => 'Hello world',
  post_tags => '{"fun", "tech", "invalid"}',
  user_id => (SELECT id FROM auth.users WHERE email = 'test_a@test.com' LIMIT 1)
);


SELECT create_post(
  body_raw => '_response_',
  reply_to_id => (SELECT post_id FROM posts WHERE title = 'testing this' LIMIT 1),
  user_id => (SELECT id FROM auth.users WHERE email = 'test_a@test.com' LIMIT 1)
);


SELECT create_thread(
  title => 'Second test',
  post_slug => '/test',
  body_raw => 'Hello worms',
  post_tags => '{"fun"}',
  user_id => (SELECT id FROM auth.users WHERE email = 'test_b@test.com' LIMIT 1)
);

SELECT create_post(
  body_raw => '_response_ to second test',
  reply_to_id => (SELECT post_id FROM posts WHERE title = 'Second test' LIMIT 1),
  user_id => (SELECT id FROM auth.users WHERE email = 'test_a@test.com' LIMIT 1)
);

SELECT create_post(
  body_raw => 'Another _response_ to second test',
  reply_to_id => (SELECT post_id FROM posts WHERE title = 'Second test' LIMIT 1),
  user_id => (SELECT id FROM auth.users WHERE email = 'test_c@test.com' LIMIT 1)
);


SELECT create_thread(
  title => 'Great tutorial',
  post_slug => '/some-pg-faker-article',
  body_raw => '**Great tutorial!** 👏 Your step-by-step guide on generating fake data with PostgreSQL is incredibly helpful. The use of the `pgfaker` extension makes the process seamless and efficient. _Testing databases just got a whole lot easier_.',
  post_tags => '{"fun", "tech"}',
  user_id => (SELECT id FROM auth.users WHERE email = 'test_a@test.com' LIMIT 1)
);

SELECT create_post(
  body_raw => 'Hey, glad you found the tutorial helpful! 😊 If you have any questions about adapting the process to different testing scenarios or need clarification on any step, feel free to ask. Happy testing! 🚀',
  reply_to_id => (SELECT post_id FROM posts WHERE title = 'Great tutorial' LIMIT 1),
  user_id => (SELECT id FROM auth.users WHERE email = 'test_c@test.com' LIMIT 1)
);

SELECT create_post(
  body_raw => 'Your insights on using custom functions for complex data scenarios are spot on! 🧠 The ability to tailor fake data generation to specific business logic adds a layer of realism to the testing environment. Customization for precision.',
  reply_to_id => (SELECT post_id FROM posts WHERE reply_to_id = (SELECT post_id FROM posts WHERE title = 'Great tutorial' LIMIT 1) LIMIT 1),
  user_id => (SELECT id FROM auth.users WHERE email = 'test_a@test.com' LIMIT 1)
);

SELECT create_thread(
  title => 'Data diversity',
  post_slug => '/some-pg-faker-article',
  body_raw => 'Kudos on highlighting the importance of data diversity in testing scenarios! 🌐 Incorporating various data types and realistic distributions with tools like Faker ensures a more robust evaluation of database performance. Diverse data, better testing.',
  post_tags => '{"fun", "tech"}',
  user_id => (SELECT id FROM auth.users WHERE email = 'test_b@test.com' LIMIT 1)
);

SELECT create_thread(
  title => 'Wow!',
  post_slug => '/some-pg-faker-article',
  body_raw => 'Time-saving tips! ⏰ The mention of the `COPY` command for bulk data insertion is a game-changer. This will definitely speed up the testing process, _especially with large datasets_. Efficiency at its best.',
  post_tags => '{"fun", "tech"}',
  user_id => (SELECT id FROM auth.users WHERE email = 'test_c@test.com' LIMIT 1)
);

-------------------------------------------------------------------------------
-- TICKETS --------------------------------------------------------------------
-------------------------------------------------------------------------------

SELECT public.create_ticket(
  user_email => 'test_a@test.com',
  body_raw => 'Hello, I just met you',
  title => 'a test ticket',
  sent_at => '2023-01-01 12:34:56.789'::TIMESTAMP(3)
);

-- should be flagged as spam
SELECT public.create_ticket(
  user_email => 'test_a@test.com',
  body_raw => 'and this is crazy',
  title => '',
  sent_at => '2023-01-01 12:34:56.789'::TIMESTAMP(3),
  name => 'name filled triggers spam'
);

-- should be flagged as spam too
SELECT public.create_ticket(
  user_email => 'test_a@test.com',
  body_raw => '',
  title => '',
  sent_at => now()::TIMESTAMP(3)
);

-- TODO:
-- 2: create admin interface to answer comments
-- 3: set realtime on that
-- 4: import mavenseed