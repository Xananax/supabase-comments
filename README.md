# Threaded Comments Prototype

Project used to test Supabase + Comments

Dependencies:
- [Supabase](https://github.com/supabase/supabase-js)
- [Lexical](https://github.com/facebook/lexical)

The site is client-only. All the app layer code is in Supabase.

Comments are saved as markdown, and then loaded down as markdown source and rendered in the client; there is no way to sanitize properly without an intermediary server, and I wasn't gonna write a markdown parser in PostgreSQL.

## How to Run

Make sure to initialize supabase. You need to have _Docker_ installed and running. Then you can run:

```bash
npm install
```
And then:
```bash
npm run supabase:reset
```

This will start supabase. Get the anon key and the service key and set them in an `.env.local` file. Look at [`env.local.template`](env.local.template) for an example.

**Note**: When running the local supabase, emails will be sent to a fake inbox. To read them, open Inbucket (running by default on `http://localhost:54324`).

To start the local dev server, run:

```bash
npm start
```

To run the site.

## TODO

- [ ] User Profile
- [ ] Gitlab pages demo (with some test free-tier supabase account)

## License

MIT

Some pieces of code have been taken from Lexical's Playground. License has been added to the headers of those files.

## Additional Reading

- [Models for hierarchical data | PPT](https://www.slideshare.net/billkarwin/models-for-hierarchical-data)
- [Manipulating Trees Using SQL and the Postgres LTREE Extension - Pat Shaughnessy](https://patshaughnessy.net/2017/12/14/manipulating-trees-using-sql-and-the-postgres-ltree-extension)
- [Looking Inside Postgres at a GiST Index - Pat Shaughnessy](https://patshaughnessy.net/2017/12/15/looking-inside-postgres-at-a-gist-index)
- [ferg1e/comment-castles: Whitelist moderated internet forum](https://github.com/ferg1e/comment-castles)
- [how to model threaded comments (e.g. reddit comments) in SQL with a simple 'ancestors' column · GitHub](https://gist.github.com/bokwoon95/4fd34a78e72b2935e78ec0f40e7e49e1)
- [Trying to Represent a Tree Structure Using Postgres - Pat Shaughnessy](https://patshaughnessy.net/2017/12/11/trying-to-represent-a-tree-structure-using-postgres)
- [Using LTree to Represent and Query Hierarchy and Tree Structures - Postgres OnLine Journal](http://www.postgresonline.com/article_pfriendly/173.html)
- [Database model for a hierarchical content](https://www.aleksandra.codes/comments-db-model)
- [Hierarchical Structures in PostgreSQL](https://hoverbear.org/blog/postgresql-hierarchical-structures/)
