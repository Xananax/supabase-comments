#!/usr/bin/env node
//@ts-check

let data = "";

process
  .openStdin()
  .on("data", (chunk) => (data += chunk))
  .on("end", () => {
    data = data
      .replace(
        /^\s*(.*?): (.*)\n/gim,
        (_, key, value) =>
          `${key.replace(/ /g, `_`).toUpperCase()}="${value}"\n`
      )
      .replace(/API_URL/, `NEXT_PUBLIC_SUPABASE_URL`)
      .replace(/DB_URL/, `SUPABASE_DATABASE_URL`)
      .replace(/anon_key/i, `NEXT_PUBLIC_SUPABASE_ANON_KEY`)
      .replace(/service_role_key/i, `SUPABASE_SERVICE_KEY`);
    console.log(data);
  });
