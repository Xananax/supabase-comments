#!/usr/bin/env node
//@ts-check
import { existsSync, readFileSync } from "fs";
import { join } from "path";

const reset = "\x1b[0m";

/**
 * Convenience literal to create DOM template elements
 * @param {TemplateStringsArray} strings
 * @param {any[]} substitutions
 */
export const tmpl = (strings, ...substitutions) =>
  strings.reduce(
    (acc, curr, index) => acc + curr + (substitutions[index] || ""),
    ""
  );

const [green, red, blue, yellow] = [
  "\x1b[32m",
  "\x1b[31m",
  "\x1b[34m",
  "\x1b[33m",
].map(
  (color) =>
    (
      /** @type {TemplateStringsArray} */ strings,
      /** @type {any} */ ...substitutions
    ) =>
      color + tmpl(strings, ...substitutions) + reset
);

function info() {
  const search = new RegExp(
    (process.argv[2] || "").split("").reduce(function (a, b) {
      return a + ".*" + b;
    }, "")
  );
  const file = join(process.cwd(), "package.json");
  /** @type {{scripts?: Record<string, string>}} */
  const pkg = JSON.parse(
    (existsSync(file) && readFileSync(file, "utf8")) || "{}"
  );
  const scripts = pkg.scripts || {};
  let title = "General";
  const scriptsInfo = Object.keys(scripts).reduce((scriptsInfo, scriptName) => {
    if (scriptName[0] === "?") {
      const key = scriptName.slice(1);
      if (key[1] === "=") {
        title = key.replace(/^=+\s*(.*?)(?:=)*$/, "$1").trim();
      } else if (key in scripts && key.match(search)) {
        const description = scripts[scriptName]
          .replace(/^\s*echo\s+/, "")
          .replace(/^(['"])(.*)\1$/, "");
        scriptsInfo[title] = scriptsInfo[title] || {};
        scriptsInfo[title][key] = description;
      }
    }
    return scriptsInfo;
  }, /** @type {Record<string, Record<string, string>>} */ ({}));
  const helpText = Object.keys(scriptsInfo)
    .map((title) =>
      [
        blue`${title}`,
        blue`${"-".repeat(title.length)}`,
        ...Object.keys(scriptsInfo[title]).map(
          (key) => green`${key}` + `\n` + `  ${scriptsInfo[title][key]}`
        ),
      ].join("\n")
    )
    .join("\n\n");
  console.log(`\nAVAILABLE SCRIPTS\n\n` + helpText + `\n`);
}

info();
